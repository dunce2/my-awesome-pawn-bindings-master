#if defined mongpawn_included_gc_
	#endinput
#endif
#define mongpawn_included_gc_

native object_ref(CollectedObject:obj);
native object_unref(CollectedObject:obj);
native object_get_reference_count(CollectedObject:obj);