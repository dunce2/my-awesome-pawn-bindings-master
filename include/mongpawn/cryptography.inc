/*
**
**			  MONG-PAWN
**		Cryptography library
**
*/

#if defined mongpawn_included_cryptography_
	#endinput
#endif
#define mongpawn_included_cryptography_

native bool:is_rdrand_available();
native gen_rand();
native randomize_array(array[], cell_count = sizeof(array));

native HashGen:create_hash_generator(const algo[]);
native hash_add_data(HashGen:generator, const data[]);
native hash_run_threaded(HashGen:generator, const callback[], const fmt[] = "", {Float, _}:...);
native hash_run(HashGen:generator, destination[], len = sizeof(destination), bool:pack = false);
native hash_get(destination[], len = sizeof(destination), bool:pack = false);
native hash_get_algo(destination[], len = sizeof(destination), bool:pack = false);
native HashGen:hash_get_current_generator();

stock CollectedObject:operator=(HashGen:ptr) return CollectedObject:ptr;