/*
**
**			  MONG-PAWN
**		Scripting main library
**
*/

#if defined mongpawn_scripting_included_
	#endinput
#endif
#define mongpawn_scripting_included_

#define	SERVER_VARTYPE_NONE                    (0)
#define SERVER_VARTYPE_INT                     (1)
#define SERVER_VARTYPE_STRING                  (2)
#define SERVER_VARTYPE_FLOAT                   (3)
#define	PLAYER_VARTYPE_NONE                    (0)
#define PLAYER_VARTYPE_INT                     (1)
#define PLAYER_VARTYPE_STRING                  (2)
#define PLAYER_VARTYPE_FLOAT                   (3)


#if !defined MP_LEGACY
	forward OnScriptInit(script_id, bool:filterscript);
	forward OnScriptExit(script_id, bool:filterscript);
#endif

#if !defined OnJITCompile
	forward OnJITCompile();
#endif

forward OnGameModeInit();
forward OnGameModeExit();

forward OnFilterScriptInit();
forward OnFilterScriptExit();

forward OnRconCommand(cmd[]);
forward OnRconLoginAttempt(ip[], password[], success);

native CallLocalFunction(const function[], const format[], {Float, _}:...);
native CallRemoteFunction(const function[], const format[], {Float, _}:...);

native SendClientMessageToAll(color, const message[]);

native SHA256_PassHash(const password[], const salt[], ret_hash[], ret_hash_len = sizeof ret_hash);

native SetSVarInt(const varname[], int_value);
native GetSVarInt(const varname[]);
native SetSVarString(const varname[], const string_value[]);
native GetSVarString(const varname[], string_return[], len = sizeof string_return, bool:pack = false);
native SetSVarFloat(const varname[], Float:float_value);
native Float:GetSVarFloat(const varname[]);
native DeleteSVar(const varname[]);
native GetSVarsUpperIndex();
native GetSVarNameAtIndex(index, ret_varname[], ret_len = sizeof ret_varname, bool:pack = false);
native GetSVarType(const varname[]);

native SetGameModeText(const string[]);

native SetTeamCount(teams);

native GameModeExit();

native SetWorldTime(hour);
native SetWeather(weatherid);
native Float:GetGravity();
native SetGravity(Float:gravity);

native CreateExplosion(Float:X, Float:Y, Float:Z, type, Float:Radius);

native EnableZoneNames(enable);

native DisableInteriorEnterExits();

/* DL FUNCTIONS */
#pragma deprecated This function does not work in RakMong
native AddCharModel(baseid, newid, const dffname[], const txdname[]);

#pragma deprecated This function does not work in RakMong
native AddSimpleModel(virtualworld, baseid, newid, const dffname[], const txdname[]);

#pragma deprecated This function does not work in RakMong
native AddSimpleModelTimed(virtualworld, baseid, newid, const dffname[], const txdname[], timeon, timeoff);

#pragma deprecated This function does not work in RakMong
native FindModelFileNameFromCRC(crc, retstr[], retstr_size = sizeof retstr);

#pragma deprecated This function does not work in RakMong
native FindTextureFileNameFromCRC(crc, retstr[], retstr_size = sizeof retstr);

#pragma deprecated This function does not work in RakMong
native RedirectDownload(playerid, const url[]);

native SendRconCommand(const command[]);

native GetConsoleVarAsString(const varname[], buffer[], len = sizeof buffer);
native GetConsoleVarAsInt(const varname[]);
native GetConsoleVarAsBool(const varname[]);
native GetServerVarAsString(const varname[], buffer[], len = sizeof buffer) = GetConsoleVarAsString;
native GetServerVarAsInt(const varname[]) = GetConsoleVarAsInt;
native GetServerVarAsBool(const varname[]) = GetConsoleVarAsBool;

native GetServerTickRate();

public const bool:JITEnabled = false;
native bool:IsJITCompiled();
