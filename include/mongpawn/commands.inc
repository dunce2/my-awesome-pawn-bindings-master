/*
**
**			  	  	   MONG-PAWN
**
*/

#if defined mongpawn_commands_included_
	#endinput
#endif
#define mongpawn_commands_included_

#define CMD_SUCCESS _CMD@RETURN:1
#define CMD_FAILURE _CMD@RETURN:0

#define _CMD@REPAIR_CALLBACK:cmd@%0(%1) cmd@%0(%1); public _CMD@RETURN:cmd@%0(%1)
#define _CMD@MAKE_CALLBACK:%0(%1) forward _CMD@RETURN:%0(%1); public _CMD@RETURN:%0(%1)
#define _CMD@FIND_ALTS:%0,%1] mpi@ca@%0(); public mpi@ca@%0() { SetCommandAlias(#%0, %1); return 1; } _CMD@MAKE_CALLBACK:cmd@%0
#define _CMD@RETURN:_CMD@REPAIR_CALLBACK:cmd@%0[ _CMD@FIND_ALTS:%0,
#define CMD:%0(%1) forward _CMD@RETURN:_CMD@REPAIR_CALLBACK:cmd@%0(%1)

#define cmd_flags:%0(%1); init_fun cf@%0() { SetCommandFlags(#%0,%1); return 1; }
#define cmd_description:%0(%1); init_fun cd@%0() { SetCommandDescription(#%0,%1); return 1; }

forward OnPlayerCommandReceived(playerid, command[], params[], bool:success, flags, const base[]);

native CallCommand(playerid, const command[], const params[] = "");
native EmulateCommand(playerid, const cmdtext[]);
native DoesCommandExist(const command[]);
native GetTotalCommandCount();
native SetCommandFlags(const command[], flags);
native GetCommandFlags(const command[]);
native SetCommandDescription(const command[], const description[]);
native GetCommandDescription(const command[], description[], len = sizeof(description));
native SetCommandAlias(const command[], ...);