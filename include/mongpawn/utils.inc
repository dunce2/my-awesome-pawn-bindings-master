/*
**
**			  	  MONG-PAWN
**		  Utilities and miscelaneous
**
*/

#if defined mongpawn_utils_included_
	#endinput
#endif
#define mongpawn_utils_included_

#if __Pawn >= 0x400
	#define MP_EMPTY_STRING ''''
#else
	#define MP_EMPTY_STRING ""
#endif

#define init_fun%2\32;%0(%1)\
	forward mpi@%0(%1);\
	public mpi@%0(%1)
	
