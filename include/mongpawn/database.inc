/*
**
**			  	  	   MONG-PAWN
**
*/

#if defined mongpawn_included_database_
	#endinput
#endif
#define mongpawn_included_database_

native DB:db_open(const name[]);
native DB:db_open_memory();
native db_close(DB:db);

native db_dump(DB:db, const destination[]);

native db_exec(DB:db, const query[]);
native DBResult:db_query(DB:db, const query[]);
native db_free_result(DBResult:dbresult);
native db_num_rows(DBResult:dbresult);
native db_next_row(DBResult:dbresult);
native db_num_fields(DBResult:dbresult);
native db_field_name(DBResult:dbresult, field, result[], maxlength = sizeof result);
native db_get_field(DBResult:dbresult, field, result[], maxlength = sizeof result);
native db_get_field_int(DBResult:result, field = 0);
native Float:db_get_field_float(DBResult:result, field = 0);
native db_get_field_assoc(DBResult:dbresult, const field[], result[], maxlength = sizeof result);
native db_get_field_assoc_int(DBResult:result, const field[]);
native Float:db_get_field_assoc_float(DBResult:result, const field[]);
native db_get_mem_handle(DB:db);
native db_get_result_mem_handle(DBResult:result);
native db_debug_openfiles();
native db_debug_openresults();
