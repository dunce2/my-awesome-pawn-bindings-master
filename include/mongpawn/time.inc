/*
**
**			  	  		MONG-PAWN
**		  Time native functions and utilities
**
*/

#if defined mongpawn_time_included_
	#endinput
#endif
#define mongpawn_time_included_

#if __Pawn >= 0x400
const DateElement: {
#else
enum DateElement {
#endif
	DATE_SINCE_EPOCH = 0,
	DATE_SECONDS,
	DATE_MINUTES,
	DATE_HOURS,
	DATE_WEEK_DAY,
	DATE_MONTH_DAY,
	DATE_YEAR_DAY,
	DATE_MONTH,
	DATE_YEAR
};

public const CLOCKS_PER_SEC = 0;

native GetTickCount();