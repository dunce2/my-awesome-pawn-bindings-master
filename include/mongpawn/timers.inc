/*
**
**			  	  	   MONG-PAWN
**		  Timer native functions and utilities
**
*/

#if defined mongpawn_timers_included_
	#endinput
#endif
#define mongpawn_timers_included_

native SetTimer(const funcname[], interval, repeating);
native SetTimerEx(const funcname[], interval, repeating, const format[], {Float, _}:...);
native KillTimer(timerid);

native Timer:timer_new(const cb[], timeout, interval, const format[] = MP_EMPTY_STRING, {Float, _}:...);

native timer_set_callback(Timer:t, const cb[], const format[] = MP_EMPTY_STRING, {Float, _}:...);
native timer_set_interval(Timer:t, interval);
native timer_get_interval(Timer:t);
native timer_set_timeout(Timer:t, timeout);
native timer_get_timeout(Timer:t);
native timer_get_due_in(Timer:t);

native timer_start(Timer:t);
native timer_stop(Timer:t);

native timer_pause(Timer:t);
native timer_resume(Timer:t);
native bool:timer_is_paused(Timer:t);

native timer_destroy(Timer:t);