/*
**
**			  	  	   MONG-PAWN
**
*/

#if defined mongpawn_included_pickups_
	#endinput
#endif
#define mongpawn_included_pickups_

native AddStaticPickup(model, type, Float:X, Float:Y, Float:Z, virtualworld = 0);
native CreatePickup(model, type, Float:X, Float:Y, Float:Z, virtualworld = 0);
native DestroyPickup(pickup);

forward OnPlayerPickUpPickup(playerid, pickupid);
