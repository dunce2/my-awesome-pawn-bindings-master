/*
**
**			  MONG-PAWN
**		 Math native library
**
*/

#if defined mongpawn_math_included_
	#endinput
#endif
#define mongpawn_math_included_

#define MP_PI 3.14159265358979323846

native Float:VectorSize(Float:x, Float:y, Float:z);
native Float:asin(Float:value);
native Float:acos(Float:value);
native Float:atan(Float:value);
native Float:atan2(Float:y, Float:x);