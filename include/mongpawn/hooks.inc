/*
**
**			      MONG-PAWN
**		  Function hooking library
**
*/

#if defined mongpawn_included_hooks_
	#endinput
#endif
#define mongpawn_included_hooks_

#include <mongpawn/utils>

native register_hook(const callback[], const hook_function[]);
native add_hook_replacement(const key[], const replacement[]);
native clear_hooks(const function[]);

#define hook_replacement%2\32;%0->%3\32;%1; forward mphrp%0(); public mphrp%0() { add_hook_replacement(#%0,#%1); }

#define hook%3\32;%0<%1>(%2) \
	init_fun %0%1() { register_hook(#%1, #%0_%1); } \
	forward %0_%1(%2); \
	public %0_%1(%2)