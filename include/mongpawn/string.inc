/*
**
**			  MONG-PAWN
**		String library include
**
*/

#if defined _inc_mong_pawn_string
	#endinput
#endif
#define _inc_mong_pawn_string

native format(destination[], size, const format[], {Float, _}:...);
native format_packed(destination[], size, const format[], {Float, _}:...);

#if __Pawn >= 0x400
const SpecifierInputType: {
#else
enum SpecifierInputType {
#endif
	INPUT_TYPE_CELL = 0,
	INPUT_TYPE_STRING,
	INPUT_TYPE_FLOAT = INPUT_TYPE_CELL
};

native register_format_specifier(character, const callback[], SpecifierInputType:input_type);