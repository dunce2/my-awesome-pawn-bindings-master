/*
**
**			  MONG-PAWN
**		Console library include
**
*/

#if defined mongpawn_console_included_
	#endinput
#endif
#define mongpawn_console_included_

native print(const message[]);
native printf(const message[], {Float, _}:...);