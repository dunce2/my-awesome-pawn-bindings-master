/*
**
**			  	  	   MONG-PAWN
**
*/

#if defined mongpawn_included_gametext_
	#endinput
#endif
#define mongpawn_included_gametext_

native GameTextForAll(const string[], time, style);
native GameTextForPlayer(playerid, const string[], time, style);