/*
**
**			  	  	   MONG-PAWN
**
*/

#if defined mongpawn_included_3dtextlabels_
	#endinput
#endif
#define mongpawn_included_3dtextlabels_

#include <mongpawn/player>
#include <mongpawn/vehicle>

#define INVALID_3DTEXT_ID                      (Text3D:0xFFFF)
#define INVALID_PLAYER_3DTEXT_ID               (PlayerText3D:0xFFFF)

native Text3D:Create3DTextLabel(const text[], color, Float:X, Float:Y, Float:Z, Float:DrawDistance, virtualworld, testLOS=0);
native Delete3DTextLabel(Text3D:id);
native Attach3DTextLabelToPlayer(Text3D:id, playerid, Float:OffsetX, Float:OffsetY, Float:OffsetZ);
native Attach3DTextLabelToVehicle(Text3D:id, vehicleid, Float:OffsetX, Float:OffsetY, Float:OffsetZ);
native Update3DTextLabelText(Text3D:id, color, const text[]);
native PlayerText3D:CreatePlayer3DTextLabel(playerid, const text[], color, Float:X, Float:Y, Float:Z, Float:DrawDistance, attachedplayer = INVALID_PLAYER_ID, attachedvehicle = INVALID_VEHICLE_ID, testLOS=0);
native DeletePlayer3DTextLabel(playerid, PlayerText3D:id);
native UpdatePlayer3DTextLabelText(playerid, PlayerText3D:id, color, const text[]);
