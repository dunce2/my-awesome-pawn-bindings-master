/*
**
**			  	  	   MONG-PAWN
**
*/

#if defined mongpawn_included_weapons_
	#endinput
#endif
#define mongpawn_included_weapons_

native GetWeaponName(weaponid, weapon[], len = sizeof weapon);
native AllowInteriorWeapons(allow);
forward OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ);
