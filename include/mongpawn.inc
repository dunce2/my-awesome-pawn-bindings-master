/*
**
**			  MONG-PAWN
**		Default library include
**
*/

#if defined mongpawn_included_
	#endinput
#endif
#define mongpawn_included_

#if __Pawn >= 0x400
	#include <pawn4/args>
	#include <pawn4/core>
	#include <pawn4/file>
	#include <pawn4/float>
	#include <pawn4/string>
	#include <pawn4/time>
#else
	#include <pawn3/args>
	#include <pawn3/core>
	#include <pawn3/file>
	#include <pawn3/float>
	#include <pawn3/string>
	#include <pawn3/time>
#endif

#if !defined USE_STANDARD_INCLUDES
	#define USE_STANDARD_INCLUDES 1
#endif

#if USE_STANDARD_INCLUDES
	#include <mongpawn/console>
	#include <mongpawn/string>
	#include <mongpawn/scripting>
	#include <mongpawn/math>
	#include <mongpawn/player>
	#include <mongpawn/vehicle>
	#include <mongpawn/weapons>
	#include <mongpawn/objects>
	#include <mongpawn/pickups>
	#include <mongpawn/database>
	#include <mongpawn/gametext>
	#include <mongpawn/textdraws>
	#include <mongpawn/menus>
	#include <mongpawn/3dtextlabels>
	#include <mongpawn/actors>
#endif