#include "Natives.hpp"
#include "Natives.hpp"
#include "Natives.hpp"
#include "../main.hpp"

cell mp::natives::gc::object_ref(std::shared_ptr<CScript> script, cell obj_ptr)
{
	void* object_ptr = reinterpret_cast<void*>(obj_ptr);
	if (!script->GC()->Exists(object_ptr))
		return 0;

	script->GC()->GetSymbol(object_ptr).ref_count++;

	return 1;
}

cell mp::natives::gc::object_unref(std::shared_ptr<CScript> script, cell obj_ptr)
{
	void* object_ptr = reinterpret_cast<void*>(obj_ptr);
	if (!script->GC()->Exists(object_ptr))
		return 0;

	script->GC()->GetSymbol(object_ptr).ref_count--;

	return 1;
}

cell mp::natives::gc::object_get_reference_count(std::shared_ptr<CScript> script, cell obj_ptr)
{
	void* object_ptr = reinterpret_cast<void*>(obj_ptr);
	if (!script->GC()->Exists(object_ptr))
		return 0;

	return script->GC()->GetSymbol(object_ptr).ref_count;
}

cell mp::natives::gc::object_exists(std::shared_ptr<CScript> script, cell obj_ptr)
{
	return script->GC()->Exists(reinterpret_cast<void*>(obj_ptr));
}

cell mp::natives::gc::gc_collect(std::shared_ptr<CScript> script)
{
	script->GC()->Collect();
	return 1;
}
