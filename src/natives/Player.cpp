#include "../main.hpp"
#include "Natives.hpp"

cell mp::natives::player::SetPVarInt(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, cell int_value)
{
	if (!player)
		return 0;

	if (varname.empty())
		return 0;

	mp::var_manager->Set(player->Id(), varname, int_value);

	return 1;
}

cell mp::natives::player::SetPVarString(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, std::string&& string_value)
{
	if (!player)
		return 0;

	if (varname.empty())
		return 0;

	mp::var_manager->Set(player->Id(), varname, string_value);

	return 1;
}

cell mp::natives::player::SetPVarFloat(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, float float_value)
{
	if (!player)
		return 0;

	if (varname.empty())
		return 0;

	mp::var_manager->Set(player->Id(), varname, float_value);

	return 1;
}

cell mp::natives::player::GetPVarInt(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname)
{
	if (!player)
		return 0;

	cell val{ 0 };
	if (!mp::var_manager->Get(player->Id(), varname, val))
	{
		mp::log->warn("Couldn't access SVar of type int with key \"{}\"", varname);
		return 0;
	}

	return val;
}

cell mp::natives::player::GetPVarString(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, cell* string_return, cell len)
{
	if (!player)
		return 0;

	std::string val;
	if (!mp::var_manager->Get(player->Id(), varname, val))
	{
		mp::log->warn("Couldn't access SVar of type string with key \"{}\"", varname);
		return 0;
	}

	amx_SetString(string_return, val.data(), 0, 0, len);

	return val.size();
}

float mp::natives::player::GetPVarFloat(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname)
{
	if (!player)
		return 0.F;

	float val{ 0.F };
	if (!mp::var_manager->Get(player->Id(), varname, val))
	{
		mp::log->warn("Couldn't access SVar of type float with key \"{}\"", varname);
		return 0;
	}

	return val;
}

cell mp::natives::player::DeletePVar(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname)
{
	if (!player)
		return 0;

	if (!mp::var_manager->Exists(player->Id(), varname))
		return 0;

	mp::var_manager->Delete(player->Id(), varname);

	return 1;
}

cell mp::natives::player::GetPVarsUpperIndex(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return mp::var_manager->UpperIndex(player->Id());
}

cell mp::natives::player::GetPVarNameAtIndex(std::shared_ptr<CScript> script, rm::player* player, cell index, cell* ret_varname, cell ret_len)
{
	if (!player)
		return 0;

	std::string name = mp::var_manager->GetNameAtIndex(player->Id(), index);
	if (name.empty())
		return 0;

	amx_SetString(ret_varname, name.c_str(), 0, 0, ret_len);
	return 1;
}

cell mp::natives::player::GetPVarType(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname)
{
	if (!player)
		return 0;

	return mp::var_manager->GetType(player->Id(), varname);
}

cell mp::natives::player::SetSpawnInfo(std::shared_ptr<CScript> script, rm::player* player, cell team, cell skin, float x, float y, float z, float rotation, cell weapon1, cell weapon1_ammo, cell weapon2, cell weapon2_ammo, cell weapon3, cell weapon3_ammo)
{
	if (!player)
		return 0;

	return player->SpawnInfo({ static_cast<unsigned char>(team), skin, '\0', { x, y, z }, rotation, { weapon1, weapon2, weapon3 }, { weapon1_ammo, weapon2_ammo, weapon3_ammo } });
}

cell mp::natives::player::SpawnPlayer(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Spawn();
}

cell mp::natives::player::SetPlayerPos(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z)
{
	if (!player)
		return 0;

	return player->Position({ x, y, z });
}

cell mp::natives::player::SetPlayerPosFindZ(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z)
{
	if (!player)
		return 0;

	return player->SetPositionAndFindZ({ x, y, z });
}

cell mp::natives::player::GetPlayerPos(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z)
{
	if (!player)
		return 0;

	auto [f_x, f_y, f_z] = player->Position();
	*x = amx::cast<cell>(f_x);
	*y = amx::cast<cell>(f_y);
	*z = amx::cast<cell>(f_z);

	return 1;
}

cell mp::natives::player::SetPlayerFacingAngle(std::shared_ptr<CScript> script, rm::player* player, float ang)
{
	if (!player)
		return 0;

	player->FacingAngle(ang);

	return 1;
}

cell mp::natives::player::GetPlayerFacingAngle(std::shared_ptr<CScript> script, rm::player* player, cell* ang)
{
	if (!player)
		return 0;

	*ang = amx::cast<cell>(player->FacingAngle());

	return 1;
}

cell mp::natives::player::IsPlayerInRangeOfPoint(std::shared_ptr<CScript> script, rm::player* player, float range, float x, float y, float z)
{
	if (!player)
		return 0;

	glm::vec3 point_two{ x, y, z };
	auto [pos_x, pos_y, pos_z] = player->Position();
	glm::vec3 point_one{ pos_x, pos_y, pos_z };

	return glm::distance(point_one, point_two) < range;
}

float mp::natives::player::GetPlayerDistanceFromPoint(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z)
{
	if (!player)
		return 0.F;

	glm::vec3 point_one{ x, y, z };
	auto [pos_x, pos_y, pos_z] = player->Position();
	glm::vec3 point_two{ pos_x, pos_y, pos_z };

	return glm::distance(point_one, point_two);
}

cell mp::natives::player::IsPlayerStreamedIn(std::shared_ptr<CScript> script, rm::player* player, rm::player* forplayer)
{
	if (!player || !forplayer)
		return 0;

	auto [player_x, player_y, player_z] = player->Position();
	auto [forplayer_x, forplayer_y, forplayer_z] = forplayer->Position();
	glm::vec3 point_one{ player_x, player_y, player_z };
	glm::vec3 point_two{ forplayer_x, forplayer_y, forplayer_z };

	// TO-DO: Change this to proper streaming checks
	return glm::distance(point_one, point_two) < 200.F;
}

cell mp::natives::player::SetPlayerInterior(std::shared_ptr<CScript> script, rm::player* player, cell interiorid)
{
	if (!player)
		return 0;

	return player->Interior(static_cast<unsigned char>(interiorid));
}

cell mp::natives::player::GetPlayerInterior(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Interior();
}

cell mp::natives::player::SetPlayerHealth(std::shared_ptr<CScript> script, rm::player* player, float health)
{
	if (!player)
		return 0;

	return player->Health(health);
}

cell mp::natives::player::GetPlayerHealth(std::shared_ptr<CScript> script, rm::player* player, cell* health)
{
	if (!player)
		return 0;

	*health = amx::cast<cell>(player->Health());

	return 1;
}

cell mp::natives::player::SetPlayerArmour(std::shared_ptr<CScript> script, rm::player* player, float armour)
{
	if (!player)
		return 0;

	return player->Armour(armour);
}

cell mp::natives::player::GetPlayerArmour(std::shared_ptr<CScript> script, rm::player* player, cell* armour)
{
	if (!player)
		return 0;

	*armour = amx::cast<cell>(player->Armour());

	return 1;
}

cell mp::natives::player::SetPlayerAmmo(std::shared_ptr<CScript> script, rm::player* player, cell weaponslot, cell ammo)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerAmmo(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0x7FFF;
}

cell mp::natives::player::GetPlayerWeaponState(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	enum {
		WEAPONSTATE_UNKNOWN = -1,
		WEAPONSTATE_NO_BULLETS,
		WEAPONSTATE_LAST_BULLET,
		WEAPONSTATE_MORE_BULLETS,
		WEAPONSTATE_RELOADING
	};

	// TO-DO: Code goes here

	return WEAPONSTATE_UNKNOWN;
}

cell mp::natives::player::GetPlayerTargetPlayer(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0xFFFF;
}

cell mp::natives::player::GetPlayerTargetActor(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0xFFFF;
}

cell mp::natives::player::SetPlayerTeam(std::shared_ptr<CScript> script, rm::player* player, cell teamid)
{
	if (!player)
		return 0;

	player->Team(teamid);

	return 1;
}

cell mp::natives::player::GetPlayerTeam(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return -1;

	return player->Team();
}

cell mp::natives::player::SetPlayerScore(std::shared_ptr<CScript> script, rm::player* player, cell score)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerScore(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerDrunkLevel(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->DrunkLevel();
}

cell mp::natives::player::SetPlayerDrunkLevel(std::shared_ptr<CScript> script, rm::player* player, cell level)
{
	if (!player)
		return 0;

	player->DrunkLevel(level);

	return 1;
}

cell mp::natives::player::SetPlayerColor(std::shared_ptr<CScript> script, rm::player* player, cell color)
{
	if (!player)
		return 0;

	player->Color(color);

	return 1;
}

cell mp::natives::player::GetPlayerColor(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Color();
}

cell mp::natives::player::SetPlayerSkin(std::shared_ptr<CScript> script, rm::player* player, cell skinid)
{
	if (!player)
		return 0;

	player->Skin(skinid);

	return 1;
}

cell mp::natives::player::GetPlayerSkin(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Skin();
}

// 0.3.DL native function
cell mp::natives::player::GetPlayerCustomSkin(std::shared_ptr<CScript> script, rm::player* player)
{
	return 0;
}

cell mp::natives::player::GivePlayerWeapon(std::shared_ptr<CScript> script, rm::player* player, cell weaponid, cell ammo)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0;
}

cell mp::natives::player::ResetPlayerWeapons(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->ResetWeapons();

	return 1;
}

cell mp::natives::player::SetPlayerArmedWeapon(std::shared_ptr<CScript> script, rm::player* player, cell weaponid)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerWeaponData(std::shared_ptr<CScript> script, rm::player* player, cell slot, cell* weapons, cell* ammo)
{
	if (!player)
		return 0;

	if (slot < 0 || slot > 12)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GivePlayerMoney(std::shared_ptr<CScript> script, rm::player* player, cell money)
{
	if (!player)
		return 0;

	player->GiveMoney(money);

	return 1;
}

cell mp::natives::player::ResetPlayerMoney(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->ResetMoney();

	return 1;
}

cell mp::natives::player::SetPlayerName(std::shared_ptr<CScript> script, rm::player* player, std::string&& name)
{
	if (!player)
		return 0;

	if (name.empty())
		return -1;

	if (name.length() > 64)
		return -1;

	return player->Name(name);
}

cell mp::natives::player::GetPlayerMoney(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Money();
}

cell mp::natives::player::GetPlayerState(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return static_cast<cell>(player->State());
}

cell mp::natives::player::GetPlayerIp(std::shared_ptr<CScript> script, rm::player* player, cell* ip, cell len)
{
	if (!player)
		return 0;

	amx_SetString(ip, player->Ip().c_str(), 0, 0, len);

	return 1;
}

cell mp::natives::player::GetPlayerPing(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Ping();
}

cell mp::natives::player::GetPlayerWeapon(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerKeys(std::shared_ptr<CScript> script, rm::player* player, cell* keys, cell* updown, cell* leftright)
{
	if (!player)
		return 0;

	auto [k, ud, lr] = player->Keys();
	*keys = k;
	*updown = ud;
	*leftright = lr;

	return 1;
}

cell mp::natives::player::GetPlayerName(std::shared_ptr<CScript> script, rm::player* player, cell* name, cell len)
{
	if (!player)
		return 0;

	amx_SetString(name, player->Name().c_str(), 0, 0, len);

	return 1;
}

cell mp::natives::player::SetPlayerTime(std::shared_ptr<CScript> script, rm::player* player, cell hour, cell minute)
{
	if (!player)
		return 0;

	player->Time(hour, minute);

	return 1;
}

cell mp::natives::player::GetPlayerTime(std::shared_ptr<CScript> script, rm::player* player, cell* hour, cell* minute)
{
	if (!player)
		return 0;

	auto [h, m] = player->Time();
	*hour = h;
	*minute = m;

	return 1;
}

cell mp::natives::player::TogglePlayerClock(std::shared_ptr<CScript> script, rm::player* player, cell toggle)
{
	if (!player)
		return 0;

	player->ClockEnabled(toggle);

	return 1;
}

cell mp::natives::player::SetPlayerWeather(std::shared_ptr<CScript> script, rm::player* player, cell weather)
{
	if (!player)
		return 0;
	
	player->Weather(weather);

	return 1;
}

cell mp::natives::player::ForceClassSelection(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->ForceClassSelection();

	return 1;
}

cell mp::natives::player::SetPlayerWantedLevel(std::shared_ptr<CScript> script, rm::player* player, cell level)
{
	if (!player)
		return 0;

	player->WantedLevel(level);

	return 1;
}

cell mp::natives::player::GetPlayerWantedLevel(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->WantedLevel();
}

cell mp::natives::player::SetPlayerFightingStyle(std::shared_ptr<CScript> script, rm::player* player, cell style)
{
	if (!player)
		return 0;

	if (style < 4 || (style > 7 && style < 15) || style > 16)
		return 0;

	player->FightingStyle(static_cast<rm::fighting_style>(style));

	return 1;
}

cell mp::natives::player::GetPlayerFightingStyle(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return static_cast<cell>(player->FightingStyle());
}

cell mp::natives::player::SetPlayerVelocity(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z)
{
	if (!player)
		return 0;

	player->Speed({ x, y, z });

	return 1;
}

cell mp::natives::player::GetPlayerVelocity(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z)
{
	if (!player)
		return 0;

	auto [s_x, s_y, s_z] = player->Speed();
	*x = amx::cast<cell>(s_x);
	*y = amx::cast<cell>(s_y);
	*z = amx::cast<cell>(s_z);

	return 1;
}

cell mp::natives::player::PlayCrimeReportForPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* suspectid, cell crimeid)
{
	if (!player || !suspectid)
		return 0;

	player->PlayCrimeReport(suspectid, crimeid);

	return 1;
}

cell mp::natives::player::PlayAudioStreamForPlayer(std::shared_ptr<CScript> script, rm::player* player, std::string&& url, float posX, float posY, float posZ, float distance, cell usepos)
{
	if (!player)
		return 0;

	player->PlayAudioStream(url, { posX, posY, posZ }, distance, static_cast<bool>(usepos));

	return 1;
}

cell mp::natives::player::StopAudioStreamForPlayer(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->StopAudioStream();

	return 1;
}

cell mp::natives::player::SetPlayerShopName(std::shared_ptr<CScript> script, rm::player* player, std::string&& shopname)
{
	if (!player)
		return 0;

	player->LoadInteriorScript(shopname);

	return 1;
}

cell mp::natives::player::SetPlayerSkillLevel(std::shared_ptr<CScript> script, rm::player* player, cell skill, cell level)
{
	if (!player)
		return 0;

	player->SkillLevel(static_cast<rm::player_skill_type>(skill), static_cast<rm::player_skill_level>(level));

	return 1;
}

cell mp::natives::player::GetPlayerSurfingVehicleID(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0xFFFF;

	// TO-DO: Code goes here

	return 0xFFFF;
}

cell mp::natives::player::GetPlayerSurfingObjectID(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0xFFFF;

	// TO-DO: Code goes here

	return 0xFFFF;
}

cell mp::natives::player::RemoveBuildingForPlayer(std::shared_ptr<CScript> script, rm::player* player, cell modelid, float fX, float fY, float fZ, float fRadius)
{
	if (!player)
		return 0;

	player->RemoveBuilding(modelid, { fX, fY, fZ }, fRadius);

	return 1;
}

cell mp::natives::player::GetPlayerLastShotVectors(std::shared_ptr<CScript> script, rm::player* player, cell* fOriginX, cell* fOriginY, cell* fOriginZ, cell* fHitPosX, cell* fHitPosY, cell* fHitPosZ)
{
	if (!player)
		return 0;

	auto [origin, hit] = player->LastShotVectors();
	*fOriginX = amx::cast<cell>(origin.x);
	*fOriginY = amx::cast<cell>(origin.y);
	*fOriginZ = amx::cast<cell>(origin.z);
	*fHitPosX = amx::cast<cell>(hit.x);
	*fHitPosY = amx::cast<cell>(hit.y);
	*fHitPosZ = amx::cast<cell>(hit.y);

	return 1;
}

cell mp::natives::player::SetPlayerAttachedObject(std::shared_ptr<CScript> script, rm::player* player, cell index, cell modelid, cell bone, float fOffsetX, float fOffsetY, float fOffsetZ, float fRotX, float fRotY, float fRotZ, float fScaleX, float fScaleY, float fScaleZ, cell materialcolor1, cell materialcolor2)
{
	if (!player)
		return 0;

	player->AttachObject(0, index, false, modelid, bone, { fOffsetX, fOffsetY, fOffsetZ }, { fRotX, fRotY, fRotZ }, { fScaleX, fScaleY, fScaleZ }, materialcolor1, materialcolor2);

	return 1;
}

cell mp::natives::player::RemovePlayerAttachedObject(std::shared_ptr<CScript> script, rm::player* player, cell index)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0;
}

cell mp::natives::player::IsPlayerAttachedObjectSlotUsed(std::shared_ptr<CScript> script, rm::player* player, cell index)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0;
}

cell mp::natives::player::EditAttachedObject(std::shared_ptr<CScript> script, rm::player* player, cell index)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::SetPlayerChatBubble(std::shared_ptr<CScript> script, rm::player* player, std::string&& text, cell color, float drawdistance, cell expiretime)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::PutPlayerInVehicle(std::shared_ptr<CScript> script, rm::player* player, rm::vehicle* vehicle, cell seatid)
{
	if (!player || !vehicle)
		return 0;

	player->PutInVehicle(vehicle, seatid);

	return 1;
}

cell mp::natives::player::GetPlayerVehicleID(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	auto* vehicle = player->Vehicle();
	if (!vehicle)
		return 0;

	return vehicle->Id();
}

cell mp::natives::player::GetPlayerVehicleSeat(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return -1;

	return player->Seat();
}

cell mp::natives::player::RemovePlayerFromVehicle(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->RemoveFromVehicle();

	return 1;
}

cell mp::natives::player::TogglePlayerControllable(std::shared_ptr<CScript> script, rm::player* player, bool toggle)
{
	if (!player)
		return 0;

	player->Controllable(toggle);

	return 1;
}

cell mp::natives::player::PlayerPlaySound(std::shared_ptr<CScript> script, rm::player* player, cell soundid, float x, float y, float z)
{
	if (!player)
		return 0;

	player->PlaySound(soundid, { x, y, z });

	return 1;
}

cell mp::natives::player::ApplyAnimation(std::shared_ptr<CScript> script, rm::player* player, std::string&& animlib, std::string&& animname, float fDelta, cell loop, cell lockx, cell locky, cell freeze, cell time, cell forcesync)
{
	if (!player)
		return 0;

	player->ApplyAnimation(animlib, animname, fDelta, static_cast<bool>(loop), { static_cast<bool>(lockx), static_cast<bool>(locky) }, freeze, time, forcesync);

	return 1;
}

cell mp::natives::player::ClearAnimations(std::shared_ptr<CScript> script, rm::player* player, bool forcesync)
{
	if (!player)
		return 1; // kye?

	player->ClearAnimations(forcesync);

	return 1;
}

cell mp::natives::player::GetPlayerAnimationIndex(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->AnimationIndex();
}

cell mp::natives::player::GetAnimationName(std::shared_ptr<CScript> script, cell index, cell* animlib, cell len1, cell* animname, cell len2)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::GetPlayerSpecialAction(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return static_cast<cell>(player->SpecialAction());
}

cell mp::natives::player::SetPlayerSpecialAction(std::shared_ptr<CScript> script, rm::player* player, cell actionid)
{
	if (!player)
		return 0;

	auto specialaction = static_cast<rm::player_special_action>(actionid);
	player->SpecialAction(specialaction);

	return 1;
}

cell mp::natives::player::DisableRemoteVehicleCollisions(std::shared_ptr<CScript> script, rm::player* player, bool disable)
{
	if (!player)
		return 0;

	player->VehicleCollisions(disable);

	return 1;
}

cell mp::natives::player::SetPlayerCheckpoint(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z, float size)
{
	if (!player)
		return 0;

	player->SetCheckpoint({ x, y, z }, size);

	return 1;
}

cell mp::natives::player::DisablePlayerCheckpoint(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->DisableCheckpoint();

	return 1;
}

cell mp::natives::player::SetPlayerRaceCheckpoint(std::shared_ptr<CScript> script, rm::player* player, cell type, float x, float y, float z, float nextx, float nexty, float nextz, float size)
{
	if (!player)
		return 0;

	player->SetRaceCheckpoint(static_cast<rm::player_race_checkpoint_type>(type), { x, y, z }, { nextx, nexty, nextz }, size);

	return 1;
}

cell mp::natives::player::DisablePlayerRaceCheckpoint(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->DisableRaceCheckpoint();
	
	return 1;
}

cell mp::natives::player::SetPlayerWorldBounds(std::shared_ptr<CScript> script, rm::player* player, float x_max, float x_min, float y_max, float y_min)
{
	if (!player)
		return 0;

	player->WorldBounds({ x_max, x_min }, { y_max, y_min });

	return 1;
}

cell mp::natives::player::SetPlayerMarkerForPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* showplayer, cell color)
{
	if (!player || !showplayer)
		return 0;

	player->SetPlayerMarker(showplayer, color);

	return 1;
}

cell mp::natives::player::ShowPlayerNameTagForPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* showplayer, bool show)
{
	if (!player || !showplayer)
		return 0;

	player->ShowPlayerNametag(showplayer, show);

	return 1;
}

cell mp::natives::player::SetPlayerMapIcon(std::shared_ptr<CScript> script, rm::player* player, cell iconid, float x, float y, float z, cell markertype, cell color, cell style)
{
	if (!player)
		return 0;

	player->SetMapIcon(iconid, { x, y, z }, markertype, color, static_cast<rm::player_map_icon_style>(style));

	return 1;
}

cell mp::natives::player::RemovePlayerMapIcon(std::shared_ptr<CScript> script, rm::player* player, cell iconid)
{
	if (!player)
		return 0;

	player->RemoveMapIcon(iconid);

	return 1;
}

cell mp::natives::player::AllowPlayerTeleport(std::shared_ptr<CScript> script, rm::player* player, bool allow)
{
	if (!player)
		return 1;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::SetPlayerCameraPos(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z)
{
	if (!player)
		return 0;

	player->Camera()->Position({ x, y, z });

	return 1;
}

cell mp::natives::player::SetPlayerCameraLookAt(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z, cell cut)
{
	if (!player)
		return 0;

	player->Camera()->LookAt({ x, y, z }, static_cast<rm::camera_cut>(cut));

	return 1;
}

cell mp::natives::player::SetCameraBehindPlayer(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	player->Camera()->SetBehindPlayer();

	return 1;
}

cell mp::natives::player::GetPlayerCameraPos(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z)
{
	if (!player)
		return 0;

	auto [cx, cy, cz] = player->Camera()->Position();
	*x = amx::cast<cell>(cx);
	*y = amx::cast<cell>(cy);
	*z = amx::cast<cell>(cz);

	return 1;
}

cell mp::natives::player::GetPlayerCameraFrontVector(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z)
{
	if (!player)
		return 0;

	auto [fx, fy, fz] = player->Camera()->FrontVector();
	*x = amx::cast<cell>(fx);
	*y = amx::cast<cell>(fy);
	*z = amx::cast<cell>(fz);

	return 1;
}

cell mp::natives::player::GetPlayerCameraMode(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return static_cast<cell>(player->Camera()->Mode());
}

cell mp::natives::player::EnablePlayerCameraTarget(std::shared_ptr<CScript> script, rm::player* player, bool enable)
{
	if (!player)
		return 0;

	player->CameraTargeting(enable);

	return 1;
}

cell mp::natives::player::GetPlayerCameraTargetObject(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0xFFFF;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerCameraTargetVehicle(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return -1;

	// TO-DO: Code goes here

	return -1;
}

cell mp::natives::player::GetPlayerCameraTargetPlayer(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return -1;

	// TO-DO: Code goes here

	return -1;
}

cell mp::natives::player::GetPlayerCameraTargetActor(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return -1;

	// TO-DO: Code goes here

	return 1;
}

float mp::natives::player::GetPlayerCameraAspectRatio(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0.F;

	return player->Camera()->AspectRatio();
}

float mp::natives::player::GetPlayerCameraZoom(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0.F;

	return player->Camera()->Zoom();
}

cell mp::natives::player::AttachCameraToObject(std::shared_ptr<CScript> script, rm::player* player, cell objectid)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::AttachCameraToPlayerObject(std::shared_ptr<CScript> script, rm::player* player, cell playerobjectid)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::InterpolateCameraPos(std::shared_ptr<CScript> script, rm::player* player, float FromX, float FromY, float FromZ, float ToX, float ToY, float ToZ, cell time, cell cut)
{
	if (!player)
		return 1;

	player->Camera()->InterpolatePos({ FromX, FromY, FromZ }, { ToX, ToY, ToZ }, time, static_cast<rm::camera_cut>(cut));

	return 1;
}

cell mp::natives::player::InterpolateCameraLookAt(std::shared_ptr<CScript> script, rm::player* player, float FromX, float FromY, float FromZ, float ToX, float ToY, float ToZ, cell time, cell cut)
{
	if (!player)
		return 1;

	player->Camera()->InterpolateLookAt({ FromX, FromY, FromZ }, { ToX, ToY, ToZ }, time, static_cast<rm::camera_cut>(cut));

	return 1;
}

cell mp::natives::player::IsPlayerConnected(std::shared_ptr<CScript> script, rm::player* player)
{
	return player != nullptr;
}

cell mp::natives::player::IsPlayerInVehicle(std::shared_ptr<CScript> script, rm::player* player, rm::vehicle* vehicle)
{
	if (!player || !vehicle)
		return 0;

	return player->Vehicle()->Id() == vehicle->Id();
}

cell mp::natives::player::IsPlayerInAnyVehicle(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->Vehicle() != nullptr;
}

cell mp::natives::player::IsPlayerInCheckpoint(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0;
}

cell mp::natives::player::IsPlayerInRaceCheckpoint(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 0;
}

cell mp::natives::player::SetPlayerVirtualWorld(std::shared_ptr<CScript> script, rm::player* player, cell worldid)
{
	if (!player)
		return 0;

	player->World(worldid);

	return 1;
}

cell mp::natives::player::GetPlayerVirtualWorld(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	return player->World();
}

cell mp::natives::player::EnableStuntBonusForPlayer(std::shared_ptr<CScript> script, rm::player* player, bool enable)
{
	if (!player)
		return 0;

	player->StuntBonuses(enable);

	return 1;
}

cell mp::natives::player::EnableStuntBonusForAll(std::shared_ptr<CScript> script, bool enable)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::TogglePlayerSpectating(std::shared_ptr<CScript> script, rm::player* player, bool enable)
{
	if (!player)
		return 0;

	player->Spectating(enable);

	return 1;
}

cell mp::natives::player::PlayerSpectatePlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* targetplayer, cell mode)
{
	if (!player || !targetplayer)
		return 0;

	player->SpectatePlayer(targetplayer, static_cast<rm::spectate_mode>(mode));

	return 1;
}

cell mp::natives::player::PlayerSpectateVehicle(std::shared_ptr<CScript> script, rm::player* player, rm::vehicle* targetvehicle, cell mode)
{
	if (!player || !targetvehicle)
		return 0;

	player->SpectateVehicle(targetvehicle, static_cast<rm::spectate_mode>(mode));

	return 1;
}

cell mp::natives::player::StartRecordingPlayerData(std::shared_ptr<CScript> script, rm::player* player, cell recordtype, std::string&& recordname)
{
	return 0;
}

cell mp::natives::player::StopRecordingPlayerData(std::shared_ptr<CScript> script, rm::player* player)
{
	return 0;
}

cell mp::natives::player::SelectTextDraw(std::shared_ptr<CScript> script, rm::player* player, cell hovercolor)
{
	if (!player)
		return 1;

	player->EnterTextdrawSelectionMode(0, true, hovercolor);

	return 1;
}

cell mp::natives::player::CancelSelectTextDraw(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 1;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::CreateExplosionForPlayer(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z, cell type, float radius)
{
	if (!player || (type < 0 || type > 13))
		return 1;
	
	player->CreateExplosion({ x, y, z }, type, radius);

	return 1;
}

cell mp::natives::player::SendClientCheck(std::shared_ptr<CScript> script, rm::player* player, cell type, cell memAddr, cell memOffset, cell byteCount)
{
	if (!player)
		return 0;

	player->ClientCheck(type, memAddr, memOffset, byteCount);

	return 1;
}

cell mp::natives::player::SendClientMessage(std::shared_ptr<CScript> script, rm::player* player, cell color, std::string&& message)
{
	if (!player)
		return 0;

	player->ClientMessage(color, message);

	return 1;
}

cell mp::natives::player::SendPlayerMessageToPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* sender, std::string&& message)
{
	if (!player || !sender)
		return 0;

	return 1;
}

cell mp::natives::player::SendPlayerMessageToAll(std::shared_ptr<CScript> script, rm::player* sender, std::string&& message)
{
	if (!sender)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::SendDeathMessage(std::shared_ptr<CScript> script, rm::player* killer, rm::player* killee, cell weapon)
{
	if (!killee)
		return 0;

	for (size_t i{ 0U }; i < PLAYER_POOL_SIZE; ++i)
	{
		auto* player = rm::rakmong->PlayerPool()[i];
		if (!player)
			continue;

		player->DeathMessage(killee, killer, weapon);
	}

	return 1;
}

cell mp::natives::player::SendDeathMessageToPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* killer, rm::player* killee, cell weapon)
{
	if (!player || !killee)
		return 0;

	player->DeathMessage(killee, killer, weapon);

	return 1;
}

cell mp::natives::player::GetMaxPlayers()
{
	// TO-DO: Code goes here
	return PLAYER_POOL_SIZE;
}

cell mp::natives::player::GetPlayerPoolSize()
{
	// TO-DO: Code goes here
	return PLAYER_POOL_SIZE;
}

cell mp::natives::player::AddPlayerClass(std::shared_ptr<CScript> script, cell modelid, float spawn_x, float spawn_y, float spawn_z, float z_angle, cell weapon1, cell weapon1_ammo, cell weapon2, cell weapon2_ammo, cell weapon3, cell weapon3_ammo)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::AddPlayerClassEx(std::shared_ptr<CScript> script, cell teamid, cell modelid, float spawn_x, float spawn_y, float spawn_z, float z_angle, cell weapon1, cell weapon1_ammo, cell weapon2, cell weapon2_ammo, cell weapon3, cell weapon3_ammo)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::ShowNameTags(std::shared_ptr<CScript> script, bool show)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::ShowPlayerMarkers(std::shared_ptr<CScript> script, cell mode)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::AllowAdminTeleport(std::shared_ptr<CScript> script, bool allow)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::SetDeathDropAmount(std::shared_ptr<CScript> script, cell amount)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::UsePlayerPedAnims()
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::SetNameTagDrawDistance(std::shared_ptr<CScript> script, float distance)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::DisableNameTagLOS()
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::LimitGlobalChatRadius(std::shared_ptr<CScript> script, float chat_radius)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::LimitPlayerMarkerRadius(std::shared_ptr<CScript> script, float marker_radius)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::ConnectNPC(std::shared_ptr<CScript> script_ptr, std::string&& name, std::string&& script)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::IsPlayerNPC(std::shared_ptr<CScript> script, rm::player* player)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::IsPlayerAdmin(std::shared_ptr<CScript> script, rm::player* player)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::Kick(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::Ban(std::shared_ptr<CScript> script, rm::player* player)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::BanEx(std::shared_ptr<CScript> script, rm::player* player, std::string&& reason)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetPlayerNetworkStats(std::shared_ptr<CScript> script, rm::player* player, cell* retstr, cell retstr_size)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::GetNetworkStats(std::shared_ptr<CScript> script, cell* retstr, cell retstr_size)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::GetPlayerVersion(std::shared_ptr<CScript> script, rm::player* player, cell* version, cell len)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::BlockIpAddress(std::shared_ptr<CScript> script, std::string&& ip_address, cell timems)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::UnBlockIpAddress(std::shared_ptr<CScript> script, std::string&& ip_address)
{
	// TO-DO: Code goes here
	return 1;
}

cell mp::natives::player::ShowPlayerDialog(std::shared_ptr<CScript> script, rm::player* player, cell dialogid, cell style, std::string&& caption, std::string&& info, std::string&& button1, std::string&& button2)
{
	if (!player)
		return 0;

	// TO-DO: Code goes here

	return 1;
}

cell mp::natives::player::gpci(std::shared_ptr<CScript> script, rm::player* player, std::string&& serial, cell maxlen)
{
	if (!player)
		return 0;
	
	// TO-DO: Code goes here

	return 1;
}

