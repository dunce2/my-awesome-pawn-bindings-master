#include "../main.hpp"
#include "Natives.hpp"

float mp::natives::math::VectorSize(std::shared_ptr<CScript> script, float x, float y, float z)
{
	return std::sqrt(x * x + y * y + z * z);
}

float mp::natives::math::asin(std::shared_ptr<CScript> script, float value)
{
	return std::asin(value) * 180.F / std::numbers::pi_v<float>;
}

float mp::natives::math::acos(std::shared_ptr<CScript> script, float value)
{
	return std::acos(value) * 180.F / std::numbers::pi_v<float>;
}

float mp::natives::math::atan(std::shared_ptr<CScript> script, float value)
{
	return std::atan(value) * 180.F / std::numbers::pi_v<float>;
}

float mp::natives::math::atan2(std::shared_ptr<CScript> script, float y, float x)
{
	return std::atan2(y, x) * 180.F / std::numbers::pi_v<float>;
}
