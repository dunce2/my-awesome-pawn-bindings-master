#include "Natives.hpp"
#include "Natives.hpp"
#include "../main.hpp"

cell mp::natives::cryptography::is_rdrand_available()
{
	return Botan::Processor_RNG::available();
}

cell mp::natives::cryptography::gen_rand()
{
	ucell number{ 0 };
	
	mp::rng->randomize(reinterpret_cast<std::uint8_t*>(&number), sizeof(number));

	// number &= ~(1 << 31);
	// if (Botan::Processor_RNG::available())
	//	number &= (1 << 31);

	return number;
}

cell mp::natives::cryptography::randomize_array(std::shared_ptr<CScript> script, cell* array, cell cell_count)
{
	std::memset(array, 0, cell_count * sizeof(cell));
	mp::rng->randomize(reinterpret_cast<std::uint8_t*>(array), cell_count * sizeof(cell));
	return 1;
}

cell mp::natives::cryptography::create_hash_generator(std::shared_ptr<CScript> script, std::string&& hash_function)
{
	CHashWork* work = new CHashWork(script, hash_function);
	script->GC()->Handle<CHashWork>(work, [](CHashWork* work) {
		delete work;
	});

	script->GC()->GetSymbol(work).ref_count = 0;

	return reinterpret_cast<cell>(work);
}

cell mp::natives::cryptography::hash_add_data(std::shared_ptr<CScript> script, cell hash_ptr, std::string&& data)
{
	if (!script->GC()->Exists(reinterpret_cast<void*>(hash_ptr)))
		return 0;

	auto* work = reinterpret_cast<CHashWork*>(hash_ptr);
	work->Data() += data;

	return 1;
}

cell mp::natives::cryptography::hash_run_threaded(std::shared_ptr<CScript> script, const cell* params)
{
	if (!script->GC()->Exists(reinterpret_cast<void*>(params[1])))
		return 0;

	auto* work = reinterpret_cast<CHashWork*>(params[1]);
	if (!work->SetCallback(params, 2, 3, 4))
		return 0;

	work->RunThreaded();

	return 1;
}

cell mp::natives::cryptography::hash_get(std::shared_ptr<CScript> script, cell* destination, cell len, bool pack)
{
	if (!mp::current_hash_work)
		return 0;

	std::string hash_str = mp::current_hash_work->Hash();
	amx_SetString(destination, hash_str.c_str(), pack, 0, len);

	return hash_str.length();
}

cell mp::natives::cryptography::hash_get_algo(std::shared_ptr<CScript> script, cell* destination, cell len, bool pack)
{
	if (!mp::current_hash_work)
		return 0;

	amx_SetString(destination, mp::current_hash_work->HashFunction()->name().c_str(), pack, 0, len);

	return 1;
}

cell mp::natives::cryptography::hash_get_current_generator(std::shared_ptr<CScript> script)
{
	return reinterpret_cast<cell>(mp::current_hash_work);
}
