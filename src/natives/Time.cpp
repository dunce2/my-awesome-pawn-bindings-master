#include "../main.hpp"

cell mp::natives::time::GetTickCount()
{
	/*
#ifdef _WIN32
	std::uint64_t cur_time;
	LARGE_INTEGER perfval, yo1;

	QueryPerformanceFrequency(&yo1);
	QueryPerformanceCounter(&perfval);

	__int64 quotient, remainder;
	quotient = ((perfval.QuadPart) / yo1.QuadPart);
	remainder = ((perfval.QuadPart) % yo1.QuadPart);
	cur_time = quotient * 1000000 + (remainder * 1000000 / yo1.QuadPart);

	return static_cast<cell>(cur_time / 1000);
#else
	static std::uint64_t initial_time{ 0U };
	timeval tp;
	if (!initial_time)
	{
		gettimeofday(&tp, 0);
		initial_time = ((tp.tv_sec) * 1000000 + (tp.tv_usec));
	}

	gettimeofday(&tp, 0);
	std::uint64_t cur_time = ((tp.tv_sec) * 1000000 + (tp.tv_usec));

	return static_cast<cell>((cur_time - initial_time) / 1000);
#endif
	*/

	// On Windows, high_resolution_clock wraps QueryPerformanceCounter. On Linux, it should give enough precision.
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}