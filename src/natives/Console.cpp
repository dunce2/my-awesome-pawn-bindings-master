#include "../main.hpp"
#include "Natives.hpp"

cell mp::natives::console::print([[maybe_unused]] std::shared_ptr<CScript> script, std::string&& message)
{
	mp::log->info("{}", message);
	return 0;
}

cell mp::natives::console::printf(std::shared_ptr<CScript> script, const cell* params)
{
	mp::log->info("{}", mp::format::format_string(script, params, 1, 2));
	return 0;
}
