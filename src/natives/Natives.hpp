#pragma once

namespace mp::natives 
{
	namespace console 
	{
		cell print(std::shared_ptr<CScript> script, std::string&& message);
		cell printf(std::shared_ptr<CScript> script, const cell* params);
	}

	namespace math
	{
		float VectorSize(std::shared_ptr<CScript> script, float x, float y, float z);
		float asin(std::shared_ptr<CScript> script, float value);
		float acos(std::shared_ptr<CScript> script, float value);
		float atan(std::shared_ptr<CScript> script, float value);
		float atan2(std::shared_ptr<CScript> script, float y, float x);
	}

	namespace time
	{
		cell GetTickCount();
	}

	namespace scripting 
	{
		cell SetSVarInt(std::shared_ptr<CScript> script, std::string&& varname, cell int_value);
		cell SetSVarString(std::shared_ptr<CScript> script, std::string&& varname, std::string&& string_value);
		cell SetSVarFloat(std::shared_ptr<CScript> script, std::string&& varname, float float_value);
		cell GetSVarInt(std::shared_ptr<CScript> script, std::string&& varname);
		cell GetSVarString(std::shared_ptr<CScript> script, std::string&& varname, cell* string_return, cell len);
		float GetSVarFloat(std::shared_ptr<CScript> script, std::string&& varname);
		cell DeleteSVar(std::shared_ptr<CScript> script, std::string&& varname);
		cell GetSVarsUpperIndex();
		cell GetSVarNameAtIndex(std::shared_ptr<CScript> script, cell index, cell* ret_varname, cell ret_len);
		cell GetSVarType(std::shared_ptr<CScript> script, std::string&& varname);
		cell SHA256_PassHash(std::shared_ptr<CScript> script, std::string&& password, std::string&& salt, cell* ret_hash, cell ret_hash_len);
		cell GameModeExit();
		cell CallLocalFunction(std::shared_ptr<CScript> script, const cell* params);
		cell CallRemoteFunction(std::shared_ptr<CScript> script, const cell* params);
		cell GetConsoleVarAsInt(std::shared_ptr<CScript> script, std::string&& varname);
		cell GetConsoleVarAsString(std::shared_ptr<CScript> script, std::string&& varname, cell* buffer, cell len);
		cell GetConsoleVarAsBool(std::shared_ptr<CScript> script, std::string&& varname);
		cell IsJITCompiled(std::shared_ptr<CScript> script);
		cell SendRconCommand(std::shared_ptr<CScript> script, std::string&& command);
		cell SetTeamCount();
		float floatstr(std::shared_ptr<CScript> script, std::string&& string);
	}

	namespace player
	{
		cell SetPVarInt(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, cell int_value);
		cell SetPVarString(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, std::string&& string_value);
		cell SetPVarFloat(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, float float_value);
		cell GetPVarInt(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname);
		cell GetPVarString(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname, cell* string_return, cell len);
		float GetPVarFloat(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname);
		cell DeletePVar(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname);
		cell GetPVarsUpperIndex(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPVarNameAtIndex(std::shared_ptr<CScript> script, rm::player* player, cell index, cell* ret_varname, cell ret_len);
		cell GetPVarType(std::shared_ptr<CScript> script, rm::player* player, std::string&& varname);

		cell SetSpawnInfo(std::shared_ptr<CScript> script, rm::player* player, cell team, cell skin, float x, float y, float z, float rotation, cell weapon1, cell weapon1_ammo, cell weapon2, cell weapon2_ammo, cell weapon3, cell weapon3_ammo);
		cell SpawnPlayer(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerPos(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z);
		cell SetPlayerPosFindZ(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z);
		cell GetPlayerPos(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z);
		cell SetPlayerFacingAngle(std::shared_ptr<CScript> script, rm::player* player, float ang);
		cell GetPlayerFacingAngle(std::shared_ptr<CScript> script, rm::player* player, cell* ang);
		cell IsPlayerInRangeOfPoint(std::shared_ptr<CScript> script, rm::player* player, float range, float x, float y, float z);
		float GetPlayerDistanceFromPoint(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z);
		cell IsPlayerStreamedIn(std::shared_ptr<CScript> script, rm::player* player, rm::player* forplayer);
		cell SetPlayerInterior(std::shared_ptr<CScript> script, rm::player* player, cell interiorid);
		cell GetPlayerInterior(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerHealth(std::shared_ptr<CScript> script, rm::player* player, float health);
		cell GetPlayerHealth(std::shared_ptr<CScript> script, rm::player* player, cell* health);
		cell SetPlayerArmour(std::shared_ptr<CScript> script, rm::player* player, float armour);
		cell GetPlayerArmour(std::shared_ptr<CScript> script, rm::player* player, cell* armour);
		cell SetPlayerAmmo(std::shared_ptr<CScript> script, rm::player* player, cell weaponslot, cell ammo); // to-do
		cell GetPlayerAmmo(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerWeaponState(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerTargetPlayer(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerTargetActor(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell SetPlayerTeam(std::shared_ptr<CScript> script, rm::player* player, cell teamid);
		cell GetPlayerTeam(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerScore(std::shared_ptr<CScript> script, rm::player* player, cell score); // to-do
		cell GetPlayerScore(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerDrunkLevel(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerDrunkLevel(std::shared_ptr<CScript> script, rm::player* player, cell level);
		cell SetPlayerColor(std::shared_ptr<CScript> script, rm::player* player, cell color);
		cell GetPlayerColor(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerSkin(std::shared_ptr<CScript> script, rm::player* player, cell skinid);
		cell GetPlayerSkin(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerCustomSkin(std::shared_ptr<CScript> script, rm::player* player);
		cell GivePlayerWeapon(std::shared_ptr<CScript> script, rm::player* player, cell weaponid, cell ammo); // to-do
		cell ResetPlayerWeapons(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerArmedWeapon(std::shared_ptr<CScript> script, rm::player* player, cell weaponid); // to-do
		cell GetPlayerWeaponData(std::shared_ptr<CScript> script, rm::player* player, cell slot, cell* weapons, cell* ammo); // to-do
		cell GivePlayerMoney(std::shared_ptr<CScript> script, rm::player* player, cell money);
		cell ResetPlayerMoney(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerName(std::shared_ptr<CScript> script, rm::player* player, std::string&& name);
		cell GetPlayerMoney(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerState(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerIp(std::shared_ptr<CScript> script, rm::player* player, cell* ip, cell len);
		cell GetPlayerPing(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerWeapon(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerKeys(std::shared_ptr<CScript> script, rm::player* player, cell* keys, cell* updown, cell* leftright);
		cell GetPlayerName(std::shared_ptr<CScript> script, rm::player* player, cell* name, cell len);
		cell SetPlayerTime(std::shared_ptr<CScript> script, rm::player* player, cell hour, cell minute);
		cell GetPlayerTime(std::shared_ptr<CScript> script, rm::player* player, cell* hour, cell* minute);
		cell TogglePlayerClock(std::shared_ptr<CScript> script, rm::player* player, cell toggle);
		cell SetPlayerWeather(std::shared_ptr<CScript> script, rm::player* player, cell weather);
		cell ForceClassSelection(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerWantedLevel(std::shared_ptr<CScript> script, rm::player* player, cell level);
		cell GetPlayerWantedLevel(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerFightingStyle(std::shared_ptr<CScript> script, rm::player* player, cell style);
		cell GetPlayerFightingStyle(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerVelocity(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z);
		cell GetPlayerVelocity(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z);
		cell PlayCrimeReportForPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* suspectid, cell crimeid);
		cell PlayAudioStreamForPlayer(std::shared_ptr<CScript> script, rm::player* player, std::string&& url, float posX, float posY, float posZ, float distance, cell usepos);
		cell StopAudioStreamForPlayer(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerShopName(std::shared_ptr<CScript> script, rm::player* player, std::string&& shopname);
		cell SetPlayerSkillLevel(std::shared_ptr<CScript> script, rm::player* player, cell skill, cell level);
		cell GetPlayerSurfingVehicleID(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerSurfingObjectID(std::shared_ptr<CScript> script, rm::player* player);
		cell RemoveBuildingForPlayer(std::shared_ptr<CScript> script, rm::player* player, cell modelid, float fX, float fY, float fZ, float fRadius);
		cell GetPlayerLastShotVectors(std::shared_ptr<CScript> script, rm::player* player, cell* fOriginX, cell* fOriginY, cell* fOriginZ, cell* fHitPosX, cell* fHitPosY, cell* fHitPosZ);
		cell SetPlayerAttachedObject(std::shared_ptr<CScript> script, rm::player* player, cell index, cell modelid, cell bone, float fOffsetX, float fOffsetY, float fOffsetZ, float fRotX, float fRotY, float fRotZ, float fScaleX, float fScaleY, float fScaleZ, cell materialcolor1, cell materialcolor2);
		cell RemovePlayerAttachedObject(std::shared_ptr<CScript> script, rm::player* player, cell index); // to-do
		cell IsPlayerAttachedObjectSlotUsed(std::shared_ptr<CScript> script, rm::player* player, cell index); // to-do
		cell EditAttachedObject(std::shared_ptr<CScript> script, rm::player* player, cell index); // to-do
		cell SetPlayerChatBubble(std::shared_ptr<CScript> script, rm::player* player, std::string&& text, cell color, float drawdistance, cell expiretime); // to-do
		cell PutPlayerInVehicle(std::shared_ptr<CScript> script, rm::player* player, rm::vehicle* vehicle, cell seatid);
		cell GetPlayerVehicleID(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerVehicleSeat(std::shared_ptr<CScript> script, rm::player* player);
		cell RemovePlayerFromVehicle(std::shared_ptr<CScript> script, rm::player* player);
		cell TogglePlayerControllable(std::shared_ptr<CScript> script, rm::player* player, bool toggle);
		cell PlayerPlaySound(std::shared_ptr<CScript> script, rm::player* player, cell soundid, float x, float y, float z);
		cell ApplyAnimation(std::shared_ptr<CScript> script, rm::player* player, std::string&& animlib, std::string&& animname, float fDelta, cell loop, cell lockx, cell locky, cell freeze, cell time, cell forcesync);
		cell ClearAnimations(std::shared_ptr<CScript> script, rm::player* player, bool forcesync);
		cell GetPlayerAnimationIndex(std::shared_ptr<CScript> script, rm::player* player);
		cell GetAnimationName(std::shared_ptr<CScript> script, cell index, cell* animlib, cell len1, cell* animname, cell len2); // to-do
		cell GetPlayerSpecialAction(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerSpecialAction(std::shared_ptr<CScript> script, rm::player* player, cell actionid);
		cell DisableRemoteVehicleCollisions(std::shared_ptr<CScript> script, rm::player* player, bool disable);
		cell SetPlayerCheckpoint(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z, float size);
		cell DisablePlayerCheckpoint(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerRaceCheckpoint(std::shared_ptr<CScript> script, rm::player* player, cell type, float x, float y, float z, float nextx, float nexty, float nextz, float size);
		cell DisablePlayerRaceCheckpoint(std::shared_ptr<CScript> script, rm::player* player);
		cell SetPlayerWorldBounds(std::shared_ptr<CScript> script, rm::player* player, float x_max, float x_min, float y_max, float y_min);
		cell SetPlayerMarkerForPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* showplayer, cell color);
		cell ShowPlayerNameTagForPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* showplayer, bool show);
		cell SetPlayerMapIcon(std::shared_ptr<CScript> script, rm::player* player, cell iconid, float x, float y, float z, cell markertype, cell color, cell style);
		cell RemovePlayerMapIcon(std::shared_ptr<CScript> script, rm::player* player, cell iconid);
		cell AllowPlayerTeleport(std::shared_ptr<CScript> script, rm::player* player, bool allow); // to-do
		cell SetPlayerCameraPos(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z);
		cell SetPlayerCameraLookAt(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z, cell cut);
		cell SetCameraBehindPlayer(std::shared_ptr<CScript> script, rm::player* player);
		cell GetPlayerCameraPos(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z);
		cell GetPlayerCameraFrontVector(std::shared_ptr<CScript> script, rm::player* player, cell* x, cell* y, cell* z);
		cell GetPlayerCameraMode(std::shared_ptr<CScript> script, rm::player* player);
		cell EnablePlayerCameraTarget(std::shared_ptr<CScript> script, rm::player* player, bool enable);
		cell GetPlayerCameraTargetObject(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerCameraTargetVehicle(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerCameraTargetPlayer(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell GetPlayerCameraTargetActor(std::shared_ptr<CScript> script, rm::player* player); // to-do
		float GetPlayerCameraAspectRatio(std::shared_ptr<CScript> script, rm::player* player);
		float GetPlayerCameraZoom(std::shared_ptr<CScript> script, rm::player* player);
		cell AttachCameraToObject(std::shared_ptr<CScript> script, rm::player* player, cell objectid); // to-do
		cell AttachCameraToPlayerObject(std::shared_ptr<CScript> script, rm::player* player, cell playerobjectid); // to-do
		cell InterpolateCameraPos(std::shared_ptr<CScript> script, rm::player* player, float FromX, float FromY, float FromZ, float ToX, float ToY, float ToZ, cell time, cell cut);
		cell InterpolateCameraLookAt(std::shared_ptr<CScript> script, rm::player* player, float FromX, float FromY, float FromZ, float ToX, float ToY, float ToZ, cell time, cell cut);
		cell IsPlayerConnected(std::shared_ptr<CScript> script, rm::player* player);
		cell IsPlayerInVehicle(std::shared_ptr<CScript> script, rm::player* player, rm::vehicle* vehicle);
		cell IsPlayerInAnyVehicle(std::shared_ptr<CScript> script, rm::player* player);
		cell IsPlayerInCheckpoint(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell IsPlayerInRaceCheckpoint(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell SetPlayerVirtualWorld(std::shared_ptr<CScript> script, rm::player* player, cell worldid);
		cell GetPlayerVirtualWorld(std::shared_ptr<CScript> script, rm::player* player);
		cell EnableStuntBonusForPlayer(std::shared_ptr<CScript> script, rm::player* player, bool enable);
		cell EnableStuntBonusForAll(std::shared_ptr<CScript> script, bool enable); // to-do
		cell TogglePlayerSpectating(std::shared_ptr<CScript> script, rm::player* player, bool enable);
		cell PlayerSpectatePlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* targetplayer, cell mode);
		cell PlayerSpectateVehicle(std::shared_ptr<CScript> script, rm::player* player, rm::vehicle* targetvehicle, cell mode);
		cell StartRecordingPlayerData(std::shared_ptr<CScript> script, rm::player* player, cell recordtype, std::string&& recordname); // to-do
		cell StopRecordingPlayerData(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell SelectTextDraw(std::shared_ptr<CScript> script, rm::player* player, cell hovercolor);
		cell CancelSelectTextDraw(std::shared_ptr<CScript> script, rm::player* player);
		cell CreateExplosionForPlayer(std::shared_ptr<CScript> script, rm::player* player, float x, float y, float z, cell type, float radius);
		cell SendClientCheck(std::shared_ptr<CScript> script, rm::player* player, cell type, cell memAddr, cell memOffset, cell byteCount);
		cell SendClientMessage(std::shared_ptr<CScript> script, rm::player* player, cell color, std::string&& message);
		cell SendPlayerMessageToPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* sender, std::string&& message); // to-do
		cell SendPlayerMessageToAll(std::shared_ptr<CScript> script, rm::player* sender, std::string&& message);
		cell SendDeathMessage(std::shared_ptr<CScript> script, rm::player* killer, rm::player* killee, cell weapon);
		cell SendDeathMessageToPlayer(std::shared_ptr<CScript> script, rm::player* player, rm::player* killer, rm::player* killee, cell weapon);
		cell GetMaxPlayers(); // to-do
		cell GetPlayerPoolSize(); // to-do
		cell AddPlayerClass(std::shared_ptr<CScript> script, cell modelid, float spawn_x, float spawn_y, float spawn_z, float z_angle, cell weapon1, cell weapon1_ammo, cell weapon2, cell weapon2_ammo, cell weapon3, cell weapon3_ammo); // to-do
		cell AddPlayerClassEx(std::shared_ptr<CScript> script, cell teamid, cell modelid, float spawn_x, float spawn_y, float spawn_z, float z_angle, cell weapon1, cell weapon1_ammo, cell weapon2, cell weapon2_ammo, cell weapon3, cell weapon3_ammo); // to-do
		cell ShowNameTags(std::shared_ptr<CScript> script, bool show); // to-do
		cell ShowPlayerMarkers(std::shared_ptr<CScript> script, cell mode); // to-do
		cell AllowAdminTeleport(std::shared_ptr<CScript> script, bool allow); // to-do
		cell SetDeathDropAmount(std::shared_ptr<CScript> script, cell amount); // to-do
		cell UsePlayerPedAnims(); // to-do
		cell SetNameTagDrawDistance(std::shared_ptr<CScript> script, float distance); // to-do
		cell DisableNameTagLOS(); // to-do
		cell LimitGlobalChatRadius(std::shared_ptr<CScript> script, float chat_radius); // to-do
		cell LimitPlayerMarkerRadius(std::shared_ptr<CScript> script, float marker_radius); // to-do
		cell ConnectNPC(std::shared_ptr<CScript> script_ptr, std::string&& name, std::string&& script); // to-do
		cell IsPlayerNPC(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell IsPlayerAdmin(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell Kick(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell Ban(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell BanEx(std::shared_ptr<CScript> script, rm::player* player, std::string&& reason); // to-do
		cell GetPlayerNetworkStats(std::shared_ptr<CScript> script, rm::player* player, cell* retstr, cell retstr_size); // to-do
		cell GetNetworkStats(std::shared_ptr<CScript> script, cell* retstr, cell retstr_size); // to-do
		cell GetPlayerVersion(std::shared_ptr<CScript> script, rm::player* player, cell* version, cell len);
		cell BlockIpAddress(std::shared_ptr<CScript> script, std::string&& ip_address, cell timems); // to-do:
		cell UnBlockIpAddress(std::shared_ptr<CScript> script, std::string&& ip_address); // to-do:
		cell NetStats_GetConnectedTime(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_MessagesReceived(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_BytesReceived(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_MessagesSent(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_BytesSent(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_MessagesRecvPerSecond(std::shared_ptr<CScript> script, rm::player* player); // to-do
		float NetStats_PacketLossPercent(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_ConnectionStatus(std::shared_ptr<CScript> script, rm::player* player); // to-do
		cell NetStats_GetIpPort(std::shared_ptr<CScript> script, rm::player* player, cell* ip_port, cell ip_port_len); // to-do
		cell ShowPlayerDialog(std::shared_ptr<CScript> script, rm::player* player, cell dialogid, cell style, std::string&& caption, std::string&& info, std::string&& button1, std::string&& button2); // to-do
		cell gpci(std::shared_ptr<CScript> script, rm::player* player, std::string&& serial, cell maxlen); // to-do
	}

	namespace timers
	{
		cell SetTimer(std::shared_ptr<CScript> script, const cell* params);
		cell SetTimerEx(std::shared_ptr<CScript> script, const cell* params);
	}

	namespace cryptography
	{
		cell is_rdrand_available();
		cell gen_rand();
		cell randomize_array(std::shared_ptr<CScript> script, cell* array, cell cell_count);

		cell create_hash_generator(std::shared_ptr<CScript> script, std::string&& hash_function);
		cell hash_add_data(std::shared_ptr<CScript> script, cell hash_ptr, std::string&& data);
		cell hash_run_threaded(std::shared_ptr<CScript> script, const cell* params);
		cell hash_get(std::shared_ptr<CScript> script, cell* destination, cell len, bool pack);
		cell hash_get_algo(std::shared_ptr<CScript> script, cell* destination, cell len, bool pack);
		cell hash_get_current_generator(std::shared_ptr<CScript> script);
	}

	namespace gc
	{
		cell object_ref(std::shared_ptr<CScript> script, cell obj_ptr);
		cell object_unref(std::shared_ptr<CScript> script, cell obj_ptr);
		cell object_get_reference_count(std::shared_ptr<CScript> script, cell obj_ptr);
		cell object_exists(std::shared_ptr<CScript> script, cell obj_ptr);
		cell gc_collect(std::shared_ptr<CScript> script);
	}
}