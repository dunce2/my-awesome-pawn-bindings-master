#include "../main.hpp"

cell mp::natives::timers::SetTimer(std::shared_ptr<CScript> script, const cell* params)
{
	auto timer = script->Timers()->New(script, true);
	if (!timer.first)
		return 0;

	CTimer* timer_ptr = timer.second;
	timer_ptr->Timeout() = params[2];
	timer_ptr->SetCallback(params, 1);
	if (params[3])
		timer_ptr->Interval() = params[2];

	timer_ptr->Run();

	return timer.first;
}

cell mp::natives::timers::SetTimerEx(std::shared_ptr<CScript> script, const cell* params)
{
	auto timer = script->Timers()->New(script, true);
	if (!timer.first)
		return 0;

	CTimer* timer_ptr = timer.second;
	timer_ptr->Timeout() = params[2];
	timer_ptr->SetCallback(params, 1, 4, 5);
	if (params[3])
		timer_ptr->Interval() = params[2];

	timer_ptr->Run();

	return timer.first;
}