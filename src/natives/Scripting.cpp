#include "../main.hpp"
#include "Natives.hpp"

cell mp::natives::scripting::SetSVarInt(std::shared_ptr<CScript> script, std::string&& varname, cell int_value)
{
	if (varname.empty())
		return 0;

	mp::var_manager->Set(varname, int_value);
	
	return 1;
}

cell mp::natives::scripting::SetSVarString(std::shared_ptr<CScript> script, std::string&& varname, std::string&& string_value)
{
	if (varname.empty())
		return 0;

	mp::var_manager->Set(varname, string_value);

	return 1;
}

cell mp::natives::scripting::SetSVarFloat(std::shared_ptr<CScript> script, std::string&& varname, float float_value)
{
	if (varname.empty())
		return 0;

	mp::var_manager->Set(varname, float_value);

	return 1;
}

cell mp::natives::scripting::GetSVarInt(std::shared_ptr<CScript> script, std::string&& varname)
{
	cell val{ 0 };
	if (!mp::var_manager->Get(varname, val))
	{
		mp::log->warn("Couldn't access SVar of type int with key \"{}\"", varname);
		return 0;
	}

	return val;
}

cell mp::natives::scripting::GetSVarString(std::shared_ptr<CScript> script, std::string&& varname, cell* string_return, cell len)
{
	std::string val;
	if (!mp::var_manager->Get(varname, val))
	{
		mp::log->warn("Couldn't access SVar of type string with key \"{}\"", varname);
		return 0;
	}

	amx_SetString(string_return, val.data(), 0, 0, len);

	return val.length();
}

float mp::natives::scripting::GetSVarFloat(std::shared_ptr<CScript> script, std::string&& varname)
{
	float val{ 0.F };
	if (!mp::var_manager->Get(varname, val))
	{
		mp::log->warn("Couldn't access SVar of type float with key \"{}\"", varname);
		return 0;
	}

	return val;
}

cell mp::natives::scripting::DeleteSVar(std::shared_ptr<CScript> script, std::string&& varname)
{
	if (!mp::var_manager->Exists(varname))
		return 0;

	mp::var_manager->Delete(varname);

	return 1;
}

cell mp::natives::scripting::GetSVarsUpperIndex()
{
	return mp::var_manager->UpperIndex();
}

cell mp::natives::scripting::GetSVarNameAtIndex(std::shared_ptr<CScript> script, cell index, cell* ret_varname, cell ret_len)
{
	std::string name = mp::var_manager->GetNameAtIndex(index);
	amx_SetString(ret_varname, name.c_str(), 0, 0, ret_len);
	return 1;
}

cell mp::natives::scripting::GetSVarType(std::shared_ptr<CScript> script, std::string&& varname)
{
	return mp::var_manager->GetType(varname);
}

cell mp::natives::scripting::SHA256_PassHash(std::shared_ptr<CScript> script, std::string&& password, std::string&& salt, cell* ret_hash, cell ret_hash_len)
{
	std::unique_ptr<Botan::HashFunction> sha_hasher = Botan::HashFunction::create("SHA-256");
	auto hash_vec = sha_hasher->process(password + salt);
	std::string hash = Botan::hex_encode(hash_vec);

	amx_SetString(ret_hash, hash.c_str(), 0, 0, ret_hash_len);

	return hash.length();
}

cell mp::natives::scripting::GameModeExit()
{
	mp::exited_gamemode = true;
	return 0;
}

cell mp::natives::scripting::CallLocalFunction(std::shared_ptr<CScript> script, const cell* params)
{
	std::string public_name = script->AMX()->GetString(script->AMX()->GetAddress(params[1]));
	if (public_name.empty())
		return 0;

	int function_idx{ 0 };
	if (script->AMX()->FindPublic(public_name.c_str(), &function_idx) != AMX_ERR_NONE)
		return 0;

	std::vector<CScript::ParamType> extracted_params;
	script->ExtractParams(params, 2, 3, extracted_params, true);
	
	CAMX::ScopeHeap rel(script->AMX());
	script->PushParamVector(extracted_params);

	cell return_value{ 0 };
	script->CallFunction<true>(function_idx, &return_value);

	return return_value;
}

cell mp::natives::scripting::CallRemoteFunction(std::shared_ptr<CScript> script, const cell* params)
{
	std::string public_name = script->AMX()->GetString(script->AMX()->GetAddress(params[1]));
	if (public_name.empty())
		return 0;

	std::vector<CScript::ParamType> extracted_params;
	script->ExtractParams(params, 2, 3, extracted_params, true);

	cell return_value{ 0 };

	mp::script_manager->ForEach([&](const std::pair<unsigned, std::shared_ptr<CScript>>& s) {
		int function_idx{ 0 };
		if (s.second->AMX()->FindPublic(public_name.c_str(), &function_idx) != AMX_ERR_NONE)
			return;

		CAMX::ScopeHeap rel(s.second->AMX());
		s.second->PushParamVector(extracted_params);
		s.second->CallFunction<true>(function_idx, &return_value);
	});

	return return_value;
}

cell mp::natives::scripting::GetConsoleVarAsInt(std::shared_ptr<CScript> script, std::string&& varname)
{
	auto var = mp::console->GetVariable(varname);
	if (!var)
		return 0;

	if (!std::holds_alternative<int>(var->get().value))
		return 0;

	return std::get<int>(var->get().value);
}

cell mp::natives::scripting::GetConsoleVarAsString(std::shared_ptr<CScript> script, std::string&& varname, cell* buffer, cell len)
{
	auto var = mp::console->GetVariable(varname);
	if (!var)
		return 0;

	if (!std::holds_alternative<std::string>(var->get().value))
		return 0;

	const std::string& var_str = std::get<std::string>(var->get().value);

	amx_SetString(buffer, var_str.c_str(), 0, 0, len);

	return var_str.length();
}

cell mp::natives::scripting::GetConsoleVarAsBool(std::shared_ptr<CScript> script, std::string&& varname)
{
	auto var = mp::console->GetVariable(varname);
	if (!var)
		return 0;

	if (!std::holds_alternative<bool>(var->get().value))
		return 0;

	return std::get<bool>(var->get().value);
}

cell mp::natives::scripting::IsJITCompiled(std::shared_ptr<CScript> script)
{
	return (script->AMX()->Flags() & AMX_FLAG_JITC) != 0;
}

cell mp::natives::scripting::SendRconCommand(std::shared_ptr<CScript> script, std::string&& command)
{
	return mp::rcon->RunCommandString(std::move(command));
}

cell mp::natives::scripting::SetTeamCount()
{
	return 1;
}

float mp::natives::scripting::floatstr(std::shared_ptr<CScript> script, std::string&& string)
{
	return std::stof(string);
}
