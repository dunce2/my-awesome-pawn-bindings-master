#pragma once

namespace mp::crypto {
	void DoHash(uv_work_t* handle);
	void AfterHash(uv_work_t* handle, int status);
}

class CHashWork
{
	std::shared_ptr<CScript> _script;
	std::unique_ptr<Botan::HashFunction> _hash_function;
	uv_work_t* _work_handle;
	std::string _hash_result;
	std::string _hash_data;
	int _callback_idx{ 0 };
	bool _callback_called{ false };
	std::vector<CScript::ParamType> _callback_params;

public:
	friend void mp::crypto::DoHash(uv_work_t* handle);
	friend void mp::crypto::AfterHash(uv_work_t* handle, int status);

	CHashWork(const std::shared_ptr<CScript>& script, const std::string& hash_algo);
	~CHashWork();

	bool SetCallback(const cell* params, int callback_idx, int format_idx, int params_idx);
	void RunThreaded();

	inline std::string Hash() const { return _hash_result; }
	inline const Botan::HashFunction* HashFunction() const { return _hash_function.get(); }
	IO_GETTER_SETTER(Data, _hash_data)
};

namespace mp {
	extern CHashWork* current_hash_work;
}