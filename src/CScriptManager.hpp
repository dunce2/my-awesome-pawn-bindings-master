#pragma once

class CScriptManager {
	std::unordered_map<unsigned, std::shared_ptr<CScript>> _scripts;
	unsigned _highest_id{ 1U };

public:
	static constexpr auto gamemode_id = 0U;

	enum call_mode : unsigned char {
		call_all,
		call_gamemode_first,
		call_filterscripts_first,
		call_filterscripts
	};
	CScriptManager();
	~CScriptManager() = default;

	unsigned Load(const std::string& name, bool filterscript);
	void Unload(const std::string& name);
	void Unload(std::size_t id);
	void UnloadAll();

	inline std::optional<std::shared_ptr<CScript>> Get(unsigned id) 
	{ 
		return (_scripts.contains(id) ? std::make_optional(_scripts.at(id)) : std::nullopt); 
	}

	inline std::optional<std::pair<unsigned, std::shared_ptr<CScript>>> Get(AMX* amx) 
	{
		const auto it = std::find_if(_scripts.begin(), _scripts.end(), 
			[=](const auto& pair) 
			{ 
				return pair.second->AMX()->RawAMX() == amx; 
			}
		);

		if (it == _scripts.end()) 
			return std::nullopt;
		
		return *it;
	}

	inline std::optional<std::shared_ptr<CScript>> GetGameMode() 
	{ 
		return (_scripts[0] != nullptr ? std::make_optional(_scripts[0]) : std::nullopt); 
	}

	template<class F>
	inline void ForEach(F&& f)
	{
		std::for_each(_scripts.begin(), _scripts.end(), f);
	}

	template<call_mode callmode = call_all, cell stop_on, class... Args>
	std::pair<int, cell> Call(const char* function_name, Args&&... args);
};

namespace mp {
	extern std::unique_ptr<CScriptManager> script_manager;
}

template<CScriptManager::call_mode callmode, cell stop_on, class ...Args>
std::pair<int, cell> CScriptManager::Call(const char* function_name, Args&& ...args)
{
	std::pair<int, cell> result{ AMX_ERR_NONE, 0 };
	cell& retval = result.first;;
	int& error = result.second;

	if constexpr (callmode == call_mode::call_all || callmode == call_mode::call_gamemode_first)
	{
		for (auto&& [id, script] : _scripts)
		{
			error = script->CallFunction(function_name, &retval, args...);
			//if (error != AMX_ERR_NONE)
			//	break;

			if (retval == stop_on)
				break;
		}
	}
	else if constexpr (callmode == call_mode::call_filterscripts_first)
	{
		for (auto it = std::next(_scripts.begin()); it != _scripts.end(); ++it)
		{
			auto& script = it->second;
			error = script->CallFunction(function_name, &retval, args...);
			//if (error != AMX_ERR_NONE)
			//	break;

			if (retval == stop_on)
				break;
		}
		
		if (_scripts[0] && retval != stop_on)
		{
			error = _scripts[0]->CallFunction(function_name, &retval, args...);
		}
	}
	else if constexpr (callmode == call_mode::call_filterscripts)
	{
		for (auto it = std::next(_scripts.begin()); it != _scripts.end(); ++it)
		{
			auto& script = it->second;
			error = script->CallFunction(function_name, &retval, args...);
			//if (error != AMX_ERR_NONE)
			//	break;

			if (retval == stop_on)
				break;
		}
	}
	
	return result;
}
