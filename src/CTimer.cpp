
#include "CTimer.hpp"

#include "main.hpp"

void CTimer::Callback(uv_timer_t* handle)
{
	if (handle->data == nullptr)
		return; // wuh?

	auto* timer = static_cast<CTimer*>(handle->data);
	if (timer->Callback() == -1)
	{
		uv_timer_stop(handle);
		return;
	}

	{
		CAMX::ScopeHeap rl(timer->Script()->AMX());
		timer->Script()->PushParamVector(timer->Params());

		timer->Script()->CallFunction(timer->Callback(), nullptr);
	}

	if (!timer->Interval() && timer->Legacy())
	{
		timer->Stop();
		timer->Killed() = true;
	}
}

CTimer::CTimer(uv_loop_t* loop, const std::shared_ptr<CScript>& script, bool is_old) 
	: _script(script), _old_timer(is_old)
{
	uv_update_time(loop);
	uv_timer_init(loop, &_timer_handle);
	_timer_handle.data = static_cast<void*>(this);
}

CTimer::~CTimer()
{
	mp::log->debug("Timer {} destroyed.", fmt::ptr(this));
	uv_timer_stop(&_timer_handle);
}

bool CTimer::SetCallback(const cell* params, int callback_idx, int format_idx, int params_idx)
{
	unsigned param_count = params[0] / sizeof(cell);
	if (param_count < callback_idx)
		return false;

	std::string callback_name = _script->AMX()->GetString(_script->AMX()->GetAddress(params[callback_idx]));
	if (callback_name.empty())
		return false;

	if (_script->AMX()->FindPublic(callback_name.c_str(), &_callback_idx) != AMX_ERR_NONE)
		return false;

	if (param_count >= params_idx)
	{
		_script->ExtractParams(params, format_idx, params_idx, _params, true);
	}

	return true;
}

void CTimer::Run()
{
	uv_timer_start(&_timer_handle, CTimer::Callback, _timeout_time, _repeat_time);
}

void CTimer::Stop()
{
	uv_timer_stop(&_timer_handle);
}
