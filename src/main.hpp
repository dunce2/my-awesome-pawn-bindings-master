﻿#pragma once

// Include the precompiled header just to fix IntelliSense
#include "pch.h"

//
// OS
#ifdef _WIN32
	#include <Windows.h>
	#include <intrin.h>
#elif defined __linux__
	#include <dlfcn.h>
	#include <sys/mman.h>
	#include <cpuid.h>
#endif

//
// Libraries
#include <amx/amx.h>
#include <rakmong/core.hpp>
#include <rakmong/server.hpp>
#include <rakmong/player.hpp>
#include <rakmong/vehicle.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/async.h>
#include <spdlog/async_logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/daily_file_sink.h>
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/args.h>
#include <toml++/toml.h>
#include <uv.h>
#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/rng.h>
#include <botan/system_rng.h>
#include <botan/processor_rng.h>
#include <glm/vec3.hpp>
#include <glm/glm.hpp>
#include <glm/common.hpp>

#if __cplusplus >= 202002L && (defined __GNUC__ || defined __clang__)
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wdeprecated-volatile"

	#include <safe_ptr.h>

	#pragma GCC diagnostic pop
#else
	#include <safe_ptr.h>
#endif

class CMongPawnServer;
namespace mp {
	extern std::shared_ptr<spdlog::logger> log;
	extern const std::unique_ptr<CMongPawnServer> server;
	extern uv_async_t async_stop_event_loop;
	extern uv_async_t async_process_task_queue;
	extern uv_async_t async_timer_collection;
	extern sf::contfree_safe_ptr<std::queue<std::function<void()>>> evloop_task_queue;
	extern std::thread event_loop_thread;
	extern unsigned current_gamemode;
	extern bool exited_gamemode;
	extern std::unique_ptr<Botan::RandomNumberGenerator> rng;
}

//
// Mong-Pawn
#include "Utils.hpp"
#include "Config.hpp"
#include "rcon/RCONCommands.hpp"
#include "rcon/CRcon.hpp"
#include "CConsole.hpp"
#include "CAMX.hpp"
#include "CGarbageCollector.hpp"
#include "CPluginManager.hpp"
#include "CTimerManager.hpp"
#include "CScript.hpp"
#include "CHashWork.hpp"
#include "CTimer.hpp"
#include "CScriptManager.hpp"
#include "NativeGenerator.hpp"
#include "natives/Natives.hpp"
#include "Formatter.hpp"
#include "CServerVariableManager.hpp"
#include "CMongPawnServer.hpp"

#if CUR_FILE_VERSION > 9
	#define MP_AMX4 1
	using AMX_FUNCSTUBNT = AMX_FUNCSTUB;
#endif