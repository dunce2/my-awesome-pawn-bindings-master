#pragma once

namespace mp::format 
{
	enum format_flags {
		zero_padding,
		left_alignment,
		right_alignment,
		centered,
		alternative_formatting,

		flag_count
	};

	std::string format_string(std::shared_ptr<CScript>& script, const cell* params, int format_idx, int params_idx);
}