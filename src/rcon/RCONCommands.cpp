
#include "RCONCommands.hpp"

#include "../main.hpp"

bool rcon::cmdlist(const std::string& params)
{
	return false;
}

bool rcon::varlist(const std::string& params)
{
	return false;
}

bool rcon::exit(const std::string& params)
{
	return false;
}

bool rcon::echo(const std::string& params)
{
	return false;
}

bool rcon::hostname(const std::string& params)
{
	return false;
}

bool rcon::gamemodetext(const std::string& params)
{
	return false;
}

bool rcon::mapname(const std::string& params)
{
	return false;
}

bool rcon::exec(const std::string& params)
{
	return false;
}

bool rcon::kick(const std::string& params)
{
	return false;
}

bool rcon::ban(const std::string& params)
{
	return false;
}

bool rcon::changemode(const std::string& params)
{
	return false;
}

bool rcon::gmx(const std::string& params)
{
	return false;
}

bool rcon::reloadbans(const std::string& params)
{
	return false;
}

bool rcon::reloadlog(const std::string& params)
{
	return false;
}

bool rcon::say(const std::string& params)
{
	return false;
}

bool rcon::players(const std::string& params)
{
	return false;
}

bool rcon::banip(const std::string& params)
{
	return false;
}

bool rcon::unbanip(const std::string& params)
{
	return false;
}

bool rcon::gravity(const std::string& params)
{
	return false;
}

bool rcon::weather(const std::string& params)
{
	return false;
}

bool rcon::loadfs(const std::string& params)
{
	return false;
}

bool rcon::weburl(const std::string& params)
{
	return false;
}

bool rcon::unloadfs(const std::string& params)
{
	return false;
}

bool rcon::reloadfs(const std::string& params)
{
	return false;
}

bool rcon::rcon_password(const std::string& params)
{
	return false;
}

bool rcon::password(const std::string& params)
{
	return false;
}

bool rcon::messageslimit(const std::string& params)
{
	return false;
}

bool rcon::ackslimit(const std::string& params)
{
	return false;
}

bool rcon::messageholelimit(const std::string& params)
{
	return false;
}

bool rcon::playertimeout(const std::string& params)
{
	return false;
}

bool rcon::language(const std::string& params)
{
	return false;
}
