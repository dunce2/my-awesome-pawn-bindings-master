#pragma once

namespace rcon {
	bool cmdlist(const std::string& params);
	bool varlist(const std::string& params);
	bool exit(const std::string& params);
	bool echo(const std::string& params);
	bool hostname(const std::string& params);
	bool gamemodetext(const std::string& params);
	bool mapname(const std::string& params);
	bool exec(const std::string& params);
	bool kick(const std::string& params);
	bool ban(const std::string& params);
	bool changemode(const std::string& params);
	bool gmx(const std::string& params);
	bool reloadbans(const std::string& params);
	bool reloadlog(const std::string& params);
	bool say(const std::string& params);
	bool players(const std::string& params);
	bool banip(const std::string& params);
	bool unbanip(const std::string& params);
	bool gravity(const std::string& params);
	bool weather(const std::string& params);
	bool loadfs(const std::string& params);
	bool weburl(const std::string& params);
	bool unloadfs(const std::string& params);
	bool reloadfs(const std::string& params);
	bool rcon_password(const std::string& params);
	bool password(const std::string& params);
	bool messageslimit(const std::string& params);
	bool ackslimit(const std::string& params);
	bool messageholelimit(const std::string& params);
	bool playertimeout(const std::string& params);
	bool language(const std::string& params);
}