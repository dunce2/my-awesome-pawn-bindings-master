#include "../main.hpp"
#include "CRcon.hpp"

std::unique_ptr<CRcon> mp::rcon = std::make_unique<CRcon>();

CRcon::CRcon()
{
	_commands["cmdlist"] = rcon::cmdlist;
	_commands["varlist"] = rcon::varlist;
	_commands["exit"] = rcon::exit;
	_commands["echo"] = rcon::echo;
	_commands["hostname"] = rcon::hostname;
	_commands["gamemodetext"] = rcon::gamemodetext;
	_commands["mapname"] = rcon::mapname;
	_commands["exec"] = rcon::exec;
	_commands["kick"] = rcon::kick;
	_commands["ban"] = rcon::ban;
	_commands["changemode"] = rcon::changemode;
	_commands["gmx"] = rcon::gmx;
	_commands["reloadbans"] = rcon::reloadbans;
	_commands["reloadlog"] = rcon::reloadlog;
	_commands["players"] = rcon::players;
	_commands["banip"] = rcon::banip;
	_commands["unbanip"] = rcon::unbanip;
	_commands["gravity"] = rcon::gravity;
	_commands["weather"] = rcon::weather;
	_commands["loadfs"] = rcon::loadfs;
	_commands["weburl"] = rcon::weburl;
	_commands["unloadfs"] = rcon::unloadfs;
	_commands["reloadfs"] = rcon::reloadfs;
	_commands["rcon_password"] = rcon::rcon_password;
	_commands["password"] = rcon::password;
	_commands["messageslimit"] = rcon::messageslimit;
	_commands["ackslimit"] = rcon::ackslimit;
	_commands["messageholelimit"] = rcon::messageholelimit;
	_commands["playertimeout"] = rcon::playertimeout;
	_commands["language"] = rcon::language;
}

bool CRcon::RunCommand(const std::string& cmdname, const std::string& param)
{
	try
	{
		return (_commands.at(cmdname))(param);
	}
	catch (const std::out_of_range& e)
	{
		mp::log->debug("[rcon] Failed to execute command \"{}\": Command not found", cmdname);
	}
	catch (const std::exception& e)
	{
		mp::log->debug("[rcon] Failed to execute command \"{}\": {}", cmdname, e.what());
	}

	return false;
}

bool CRcon::RunCommandString(const std::string& command)
{
	if (command[0] == ' ')
		return false;

	size_t next_space = command.find_first_of(' ');
	if (next_space == std::string::npos)
		return RunCommand(command, "");

	std::string cmdname = command.substr(0, next_space);
	if ((next_space + 1) == command.length())
		return RunCommand(cmdname, "");

	std::string params(&command[next_space + 1]);
	if(std::count(params.begin(), params.end(), ' ') == params.length())
		return RunCommand(cmdname, "");

	return RunCommand(cmdname, params);
}

void CRcon::RegisterCommand(const std::string& cmdname, callback_t callback)
{
	_commands[cmdname] = callback;
}