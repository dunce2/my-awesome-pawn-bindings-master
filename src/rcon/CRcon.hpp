#pragma once

class CRcon 
{
public:
	using callback_t = std::function<bool(const std::string&)>;

private:
	std::unordered_map<std::string, callback_t> _commands;

public:
	CRcon();

	bool RunCommand(const std::string& cmdname, const std::string& param);
	bool RunCommandString(const std::string& command);
	void RegisterCommand(const std::string& cmdname, callback_t callback);
};

namespace mp {
	extern std::unique_ptr<CRcon> rcon;
}