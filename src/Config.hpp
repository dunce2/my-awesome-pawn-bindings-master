#pragma once

namespace mp::config {
	#define feature_list \
		X(legacy_mode),\
		X(script_callbacks),\
		X(jit_compilation)

	#define X(v) v
	enum config_features : unsigned short {
		feature_list,

		feature_count
	};
	#undef X
	
	#define X(v) #v
	inline constexpr const char* feature_names[] = {
		feature_list
	};
	#undef X

	inline constexpr std::string_view config_file_path = "pawn/config.toml";
	extern std::vector<std::string> gamemodes;
	extern std::vector<std::string> filterscripts;
	extern std::vector<std::string> plugins;
	extern std::bitset<config_features::feature_count> features;

	bool load_and_parse();
}