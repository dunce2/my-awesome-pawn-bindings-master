#pragma once

// Native Generator made by katursis in his project, samp-ptl:
// https://github.com/katursis/samp-ptl

namespace mp::natives {
	struct CastableCell {
		operator cell() { return value; }
		operator float() { return amx::cast<float>(value); }
		operator cell* () { return amx->GetAddress(value); }
		operator std::string() { return std::move(CAMX::GetString(amx->GetAddress(value))); }
		operator rm::player* () { return rm::rakmong->PlayerPool()[value]; }
		operator rm::vehicle* () { return rm::rakmong->VehiclePool()[value]; }

		template<class T>
		operator T() { return static_cast<T>(value); }

		cell value;
		CAMX* amx;
	};

	inline std::unordered_map<AMX_NATIVE, std::string> _natives;

	inline std::string GetNativeName(AMX_NATIVE native) { return _natives.at(native); }

#pragma region Native generators

	template<class A, A>
	struct NativeGenerator;

	// native generator without parameter unpacking
	template<class T, auto function>
	struct NativeGenerator<T(*)(std::shared_ptr<CScript>, const cell*), function>
	{
		static cell Native(AMX* amx, const cell* params)
		{
			try
			{
				auto opt_script = mp::script_manager->Get(amx);
				if (!opt_script)
				{
					mp::log->error("[amx:{}] Native function called from unknown script.", static_cast<void*>(amx));
					return 0;
				}

				auto& script = opt_script->second;

				if constexpr (std::is_floating_point_v<T>)
				{
					return amx::cast<cell>(function(script, params));
				}

				return function(script, params);
			}
			catch (const std::exception& e)
			{
				mp::log->error("Failed to execute native function \"{}\". Exception details:", GetNativeName(Native));
				mp::log->error("   {}", e.what());
				mp::log->debug("   |- AMX pointer: {}", static_cast<void*>(amx));
				mp::log->debug("   |- params bytes: {}", params[0]);
				mp::log->debug("   |- instruction pointer: {}", amx->cip);
			}

			return 0;
		}
	};

	template<class T, class... Args, auto function>
	struct NativeGenerator<T(*)(std::shared_ptr<CScript>, Args...), function>
	{
		template<std::size_t... idx>
		inline static cell Call(std::shared_ptr<CScript>& script, const cell* params, std::index_sequence<idx...>)
		{
			if constexpr (std::is_floating_point_v<T>)
			{
				return amx::cast<cell>(function(script, CastableCell{ params[idx + 1], script->AMX() }...));
			}

			return function(script, CastableCell{ params[idx + 1], script->AMX() }...);
		}

		static cell Native(AMX* amx, const cell* params)
		{
			auto opt_script = mp::script_manager->Get(amx);
			if (!opt_script)
			{
				mp::log->error("[amx:{}] Native function called from unknown script.", static_cast<void*>(amx));
				return 0;
			}

			auto& script = opt_script->second;

			try
			{
				if (sizeof...(Args) != (params[0] / sizeof(cell)))
				{
					throw std::runtime_error{ fmt::format("Expected parameter count mismatch. Expected {}, received {}", sizeof...(Args), (params[0] / sizeof(cell))) };
				}

				return Call(script, params, std::make_index_sequence<sizeof...(Args)>{});
			}
			catch (const std::exception& e)
			{
				mp::log->error("[script:{}] Failed to execute native function \"{}\". Exception details:", script->Name(), GetNativeName(Native));
				mp::log->error("   {}", e.what());
				mp::log->debug("   |- AMX pointer: {}", static_cast<void*>(amx));
				mp::log->debug("   |- params bytes: {}", params[0]);
				mp::log->debug("   |- instruction pointer: {}", amx->cip);
			}

			return 0;
		}
	};

	template<class T, auto function>
	struct NativeGenerator<T(*)(), function>
	{
		static cell Native(AMX* amx, const cell* params)
		{
			try
			{
				if constexpr (std::is_floating_point_v<T>)
				{
					return amx::cast<cell>(function());
				}

				return function();
			}
			catch (const std::exception& e)
			{
				const auto op_script = mp::script_manager->Get(amx);

				mp::log->error("[script:{}] Failed to execute native function \"{}\". Exception details:", (op_script ? op_script->second->Name() : "unknown"), GetNativeName(Native));
				mp::log->error("   {}", e.what());
				mp::log->debug("   |- AMX pointer: {}", static_cast<void*>(amx));
				mp::log->debug("   |- params bytes: {}", params[0]);
				mp::log->debug("   |- instruction pointer: {}", amx->cip);
			}

			return 0;
		}
	};

#pragma endregion

	template<auto func>
	inline void RegisterNative(const char* function_name)
	{
		_natives[NativeGenerator<
			typename std::add_pointer_t<
				typename std::remove_pointer_t<decltype(func)>
			>,
		func>::Native] = function_name;
	}

	void RegisterAllNatives();

	inline void LoadNatives(CAMX* amx)
	{
		AMX_NATIVE_INFO native_list;
		for (auto&& [native, name] : _natives)
		{
			native_list = { name.c_str(), native };
			amx_Register(amx->RawAMX(), &native_list, 1);
		}
	}
}