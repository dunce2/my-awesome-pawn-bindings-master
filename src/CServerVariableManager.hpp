#pragma once

class CServerVariableManager {
public:
	using VarType = std::variant<cell, std::string, float>;

private:
	std::vector<std::pair<std::string, VarType>> _svars;
	std::array<std::vector<std::pair<std::string, VarType>>, PLAYER_POOL_SIZE> _pvars;

public:
	enum var_type {
		var_type_none = 0,
		var_type_int = 1,
		var_type_string = 2,
		var_type_float = 3
	};

	CServerVariableManager() = default;
	~CServerVariableManager() = default;

	template<class T>
	void Set(const std::string& key, T value)
	{
		_svars.push_back({ key, value });
	}

	void Delete(const std::string& key);

	bool Exists(const std::string& key);

	template<class T>
	bool Get(const std::string& key, T& val)
	{
		auto it = std::find_if(_svars.begin(), _svars.end(), [&](const std::pair<std::string, VarType>& v) {
			return v.first == key;
		});

		if (it == _svars.end())
			return false;

		if (!std::holds_alternative<T>(it->second))
			return false;

		val = std::get<T>(it->second);

		return true;
	}

	var_type GetType(const std::string& key);

	inline std::size_t UpperIndex()
	{
		return (_svars.empty() ? 0 : _svars.size() - 1);
	}

	std::string GetNameAtIndex(std::size_t index);

	template<class T>
	void Set(std::size_t playerid, const std::string& key, T value)
	{
		_pvars[playerid].push_back({ key, value });
	}

	void Delete(std::size_t playerid, const std::string& key);

	bool Exists(std::size_t playerid, const std::string& key);

	template<class T>
	bool Get(std::size_t playerid, const std::string& key, T& val)
	{
		auto it = std::find_if(_pvars[playerid].begin(), _pvars[playerid].end(), [&](const std::pair<std::string, VarType>& v) {
			return v.first == key;
		});

		if (it == _pvars[playerid].end())
			return false;

		if (!std::holds_alternative<T>(it->second))
			return false;

		val = std::get<T>(it->second);

		return true;
	}

	var_type GetType(std::size_t playerid, const std::string& key);

	inline std::size_t UpperIndex(std::size_t playerid)
	{
		return (_pvars[playerid].empty() ? 0 : _pvars[playerid].size() - 1);
	}

	std::string GetNameAtIndex(std::size_t playerid, std::size_t index);
};

namespace mp {
	extern std::unique_ptr<CServerVariableManager> var_manager;
}