#pragma once

class CScript {
	std::unique_ptr<CAMX> _amx;
	std::string _script_name;
	bool _filterscript{ false };
	std::unique_ptr<CTimerManager> _timers;
	std::unique_ptr<CGarbageCollector> _gc;

public:
	using ParamType = std::variant<cell, std::vector<cell>>;

	CScript(const std::filesystem::path& script_path, bool filterscript) 
		: _script_name(script_path.stem().string()), _filterscript(filterscript)
	{
		mp::log->debug("Loading new script at \"{}\"", script_path.string());

		_amx = std::make_unique<CAMX>(script_path);
		_timers = std::make_unique<CTimerManager>();
		_gc = std::make_unique<CGarbageCollector>();

		if (!(_amx->Flags() & AMX_FLAG_NTVREG))
		{
			AMX_HEADER* hdr = _amx->Header();
			auto* natives = reinterpret_cast<AMX_FUNCSTUBNT*>(hdr->natives + reinterpret_cast<unsigned>(hdr));
			auto* libraries = reinterpret_cast<AMX_FUNCSTUBNT*>(hdr->libraries + reinterpret_cast<unsigned>(hdr));

			mp::log->error("[script:{}] Missing native functions:", _script_name);
			for (AMX_FUNCSTUBNT* native = natives; native < libraries; ++native)
			{
				if (!native->address)
				{
					const auto* name = reinterpret_cast<const char*>(reinterpret_cast<unsigned char*>(hdr) + native->nameofs);
					mp::log->error("[script:{}]   {}", _script_name, std::string(name));
				}
			}

			throw std::runtime_error{ CAMX::GetErrorMessage(AMX_ERR_NOTFOUND) };
		}
	}

	void Cleanup();

	inline CAMX* AMX() { return _amx.get(); }
	inline bool FilterScript() const { return _filterscript; }
	inline std::string Name() const { return _script_name; }
	inline CGarbageCollector* GC() { return _gc.get(); }
	IO_GETTER_SETTER(Timers, _timers)

	void ExtractParams(const cell* params, int format_idx, int params_idx, std::vector<ParamType>& destination, bool reverse = false);

	template<bool skip_index_checks = false, class... Args>
	int CallFunction(int function_idx, cell* return_value, Args&&... args)
	{
		if constexpr (!skip_index_checks)
		{
			const AMX_HEADER* hdr = _amx->Header();
			const auto table_size = (hdr->natives - hdr->publics) / hdr->defsize;
			if (function_idx < 0 || function_idx > table_size)
			{
				return AMX_ERR_NOTFOUND;
			}
		}

		cell hea = _amx->PushAll(args...);
		int error = _amx->Exec(return_value, function_idx);
		_amx->Release(hea);

		return error;
	}

	template<class... Args>
	int CallFunction(const char* function_name, cell* return_value, Args&&... args)
	{
		int function_idx{ 0 };
		int error = _amx->FindPublic(function_name, &function_idx);
		if (error != AMX_ERR_NONE)
			return error;

		return CallFunction<true>(function_idx, return_value);
	}

	void PushParamVector(const std::vector<ParamType>& values);
};