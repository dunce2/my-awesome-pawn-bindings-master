﻿#include "main.hpp"

std::shared_ptr<spdlog::logger> mp::log;
const std::unique_ptr<CMongPawnServer> mp::server = std::make_unique<CMongPawnServer>();
uv_async_t mp::async_stop_event_loop;
uv_async_t mp::async_process_task_queue;
uv_async_t mp::async_timer_collection;
sf::contfree_safe_ptr<std::queue<std::function<void()>>> mp::evloop_task_queue;
std::thread mp::event_loop_thread;
unsigned mp::current_gamemode = 0U;
bool mp::exited_gamemode = false;
std::unique_ptr<Botan::RandomNumberGenerator> mp::rng;