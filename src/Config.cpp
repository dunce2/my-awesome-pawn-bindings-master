#include "main.hpp"
#include "Config.hpp"

std::vector<std::string> mp::config::gamemodes;
std::vector<std::string> mp::config::filterscripts;
std::vector<std::string> mp::config::plugins;
std::bitset<mp::config::feature_count> mp::config::features;

template<class T>
static void copy_toml_array(toml::array* arr, std::vector<T>& destination)
{
	if (!arr)
		return;

	destination.reserve(arr->size());

	for (auto&& elem : *arr)
	{
		if constexpr (toml::is_integer<T>)
		{
			if (!elem.is_integer())
				continue;
		}
		else if constexpr (toml::is_string<T>)
		{
			if (!elem.is_string())
				continue;
		}

		destination.push_back(*elem.value<T>());
	}

	destination.shrink_to_fit();
}

bool mp::config::load_and_parse()
{
	toml::table tbl;

	try
	{
		tbl = toml::parse_file(config_file_path);
	}
	catch (const toml::parse_error& e)
	{
		toml::table feature_table;
		for (size_t i{ 0U }; i < feature_count; ++i)
		{
			feature_table.insert_or_assign(mp::config::feature_names[i], false);
		}

		*(feature_table.get(mp::config::feature_names[legacy_mode])->as_boolean()) = true;

		tbl = toml::table{{
			{
				"config", toml::table{{
					{ "gamemodes", toml::array() },
					{ "filterscripts", toml::array() },
					{ "plugins", toml::array() },
					{ "log_level", static_cast<std::underlying_type_t<spdlog::level::level_enum>>(spdlog::level::info) },
				}},
			},
			{
				"features", feature_table
			}
		}};

		std::ofstream config_fstream{ config_file_path.data() };
		config_fstream << tbl;
		config_fstream.close();

		mp::log->error("[config] Failed to parse config file at \"{}\":", config_file_path);
		mp::log->error("[config]    {}", e.description());
		if (e.source().begin.line != 0)
		{
			mp::log->error("[config]    While parsing file region:");
			mp::log->error("[config]       Begin pointer: L{}-C{}", e.source().begin.line, e.source().begin.column);
			mp::log->error("[config]       End pointer: L{}-C{}", e.source().end.line, e.source().end.column);
		}
		mp::log->error("[config] Default values have been written to file if possible.");

		return false;
	}

	if (!tbl["config"].is_table())
	{
		mp::log->critical("[config] Couldn't parse file: couldn't find main table (malformed configuration)");
		return false;
	}

	auto& maintbl = *tbl["config"].as_table();
	if (maintbl["gamemodes"].is_array())
	{
		copy_toml_array(maintbl["gamemodes"].as_array(), gamemodes);
	}

	if (maintbl["filterscripts"].is_array())
	{
		copy_toml_array(maintbl["filterscripts"].as_array(), filterscripts);
	}

	if (maintbl["plugins"].is_array())
	{
		copy_toml_array(maintbl["plugins"].as_array(), plugins);
	}

	mp::log->set_level(static_cast<spdlog::level::level_enum>(maintbl["log_level"].value_or<std::underlying_type_t<spdlog::level::level_enum>>(spdlog::level::info)));

	if (tbl["features"].is_table())
	{
		auto& feature_table = *tbl["features"].as_table();

		mp::log->debug("[config] feature list:");
		for (size_t i{ 0U }; i < feature_count; ++i)
		{
			features.set(i, feature_table[mp::config::feature_names[i]].value_or<bool>(false));
			mp::log->debug("[config] {} = {}", mp::config::feature_names[i], features.test(i));
		}
	}
	else
	{
		features.set(legacy_mode, true);
	}

	return true;
}
