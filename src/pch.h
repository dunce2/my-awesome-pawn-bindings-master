#pragma once

#ifdef __cplusplus

//
// STL
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <filesystem>
#include <stdexcept>
#include <new>
#include <memory>
#include <optional>
#include <variant>
#include <queue>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <thread>
#include <bitset>
#include <array>
#include <regex>
#include <type_traits>
#include <bit>
#include <chrono>
#include <future>
#include <numbers>
#include <ranges>

#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdarg>
#include <cstdint>

#endif

#ifdef _WIN32
	#include <Windows.h>
#elif defined __linux__
	#include <dlfcn.h>
	#include <sys/mman.h>
#endif