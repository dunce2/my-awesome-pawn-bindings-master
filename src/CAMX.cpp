#include "main.hpp"
#include "CAMX.hpp"

int CAMX::Relocate()
{
	_amx->flags |= AMX_FLAG_BROWSE;
	_amx->sysreq_d = 0;

	AMX_HEADER* hdr = Header();
	unsigned char* code = _amx->base + hdr->cod;
	int32_t codesize = hdr->dat - hdr->cod;
	cell* opcode_list;
	int opcode_count = 0;
	int reloc_count = 0;
	unsigned cip = 0;
	amx::opcode_list opcode;

	Exec(reinterpret_cast<cell*>(static_cast<void*>(&opcode_list)), 0);

	_amx->flags &= ~AMX_FLAG_BROWSE;

	while (cip < codesize)
	{
		opcode = *reinterpret_cast<amx::opcode_list*>(code + cip);
		if (opcode < 0 || opcode >= amx::opcode_list::OP_NUM_OPCODES)
		{
			return AMX_ERR_INVINSTR;
		}

		const amx::opcode& op = amx::opcodes[opcode];

		(*reinterpret_cast<cell*>(code + cip)) = opcode_list[opcode];
		opcode_count++;

		cip += sizeof(cell);

		if (op.needs_relocation)
			reloc_count++;

		if (op.num_params == INT_MIN) [[unlikely]]
		{
			cip += *reinterpret_cast<cell*>(code + cip) + sizeof(cell);
		}
		else if (opcode == amx::opcode_list::OP_CASETBL)
		{
			cell count = (*reinterpret_cast<cell*>(code + cip));
			reloc_count += count;
			cip += ((2 * count + 1) * sizeof(cell)) + sizeof(cell);
		}
		else
		{
			cip += op.num_params * sizeof(cell);
		}
	}

	_amx->code_size = getMaxCodeSize() * opcode_count + hdr->cod + (hdr->stp - hdr->dat);
	_amx->reloc_size = 2 * sizeof(cell) * reloc_count;
	_amx->flags |= AMX_FLAG_RELOC;

	return AMX_ERR_NONE;
}

CAMX::CAMX(const std::filesystem::path pcode_path) : _amx(std::make_unique<AMX>())
{
	std::memset(_amx.get(), 0, sizeof(AMX));

	std::ifstream file{ pcode_path, std::ios::binary };
	if (!file.good())
		throw std::runtime_error{ CAMX::GetErrorMessage(AMX_ERR_NOTFOUND) };

	AMX_HEADER hdr{};
	file.read(reinterpret_cast<char*>(&hdr), sizeof(AMX_HEADER));
	if (file.gcount() < sizeof(AMX_HEADER))
		throw std::runtime_error{ CAMX::GetErrorMessage(AMX_ERR_FORMAT) };

	if constexpr (std::endian::native == std::endian::big)
	{
		hdr.magic = amx::swapbytes(hdr.magic);
		hdr.size = amx::swapbytes(hdr.size);
		hdr.stp = amx::swapbytes(hdr.stp);
	}

	if (hdr.magic != AMX_MAGIC)
		throw std::runtime_error{ CAMX::GetErrorMessage(AMX_ERR_FORMAT) };

	_main_address = hdr.cip;

	char* mem = new char[hdr.stp];

	file.seekg(0, std::ios::beg);
	file.read(mem, hdr.size);

	if (file.gcount() < hdr.size)
	{
		delete[] mem;
		throw std::runtime_error{ CAMX::GetErrorMessage(AMX_ERR_FORMAT) };
	}

	file.close();

	int result = amx_Init(_amx.get(), mem);
	if (result != AMX_ERR_NONE)
	{
		delete[] mem;
		throw std::runtime_error{ CAMX::GetErrorMessage(result) };
	}

	amx_CoreInit(_amx.get());
	amx_StringInit(_amx.get());
	amx_FloatInit(_amx.get());
	amx_TimeInit(_amx.get());
	amx_FileInit(_amx.get());

	mp::natives::LoadNatives(this);

	SetPublicVariable("JITEnabled", mp::config::features.test(mp::config::jit_compilation));
}

CAMX::~CAMX()
{
	if (_amx)
	{
		amx_CoreCleanup(_amx.get());
		amx_StringCleanup(_amx.get());
		amx_FloatCleanup(_amx.get());
		amx_TimeCleanup(_amx.get());
		amx_FileCleanup(_amx.get());

		if (_amx->flags & AMX_FLAG_JITC)
		{
#ifdef _WIN32
			VirtualFree(_amx->base, 0, MEM_RELEASE);
#elif defined __linux__
			munmap(_amx->base, _amx->code_size);
#endif
		}
		else
		{
			delete[] _amx->base;
		}
	}
}

int CAMX::Compile()
{
	auto* hdr = Header();

	SetCallback(amx::callback_no_sysreq_d);

	int callback_idx{ 0 };
	if (FindPublic("OnJITCompile", &callback_idx) == AMX_ERR_NONE)
	{
		cell retval{ 1 };
		if (Exec(&retval, callback_idx) != AMX_ERR_NONE || retval == 0)
		{
			mp::log->info("[jit] compilation disabled");
			
			SetCallback(amx_Callback);
			
			return AMX_ERR_NONE;
		}
	}

	_amx->flags |= AMX_FLAG_JITC;
	std::vector<std::uint8_t> code_copy(_amx->base + hdr->cod, _amx->base + hdr->dat);

	int err = Relocate();
	if (err != AMX_ERR_NONE)
	{
		std::copy(code_copy.begin(), code_copy.end(), _amx->base + hdr->cod);

		_amx->flags &= ~AMX_FLAG_JITC;
		SetCallback(amx_Callback);

		mp::log->error("[jit] relocation error: {}", CAMX::GetErrorMessage(err));
		mp::log->error("[jit] compilation disabled");
		return err;
	}

	char* native_code = nullptr;
	char* reloc_table = nullptr;

	const auto error_exit = [&, this](int error_code) {
		delete[] native_code;
		delete[] reloc_table;

		mp::log->error("[jit] compilation error: {}", CAMX::GetErrorMessage(error_code));
		mp::log->error("[jit] compilation disabled");

		std::copy(code_copy.begin(), code_copy.end(), _amx->base + hdr->cod);

		_amx->flags &= ~AMX_FLAG_JITC;
		SetCallback(amx_Callback);

		return error_code;
	};

	try
	{
		native_code = new char[_amx->code_size];
		if (_amx->reloc_size > 0)
		{
			reloc_table = new char[_amx->reloc_size];
		}
	}
	catch (const std::bad_alloc& e)
	{
		return error_exit(AMX_ERR_MEMORY);
	}

	unsigned char* old_base = _amx->base;

	err = InitJIT(static_cast<void*>(reloc_table), static_cast<void*>(native_code));
	if (err != AMX_ERR_NONE)
	{
		return error_exit(err);
	}

	delete[] reloc_table;
	reloc_table = nullptr;

#ifdef _WIN32
	_amx->base = static_cast<unsigned char*>(VirtualAlloc(nullptr, _amx->code_size, MEM_COMMIT, PAGE_EXECUTE_READWRITE));
	if(!_amx->base)
#elif defined __linux__
	_amx->base = static_cast<unsigned char*>(mmap(nullptr, _amx->code_size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
	if(_amx->base == MAP_FAILED)
#endif
	{
		_amx->base = old_base;
		return error_exit(AMX_ERR_MEMORY);
	}

	std::memcpy(_amx->base, native_code, _amx->code_size);

	delete[] old_base;
	delete[] native_code;

	return AMX_ERR_NONE;
}

std::string CAMX::GetString(const cell* amx_str)
{
	int str_len{ 0 };
	std::string result;
	amx_StrLen(amx_str, &str_len);

	if (str_len > 0)
	{
		str_len++;
		result.resize(str_len);
		amx_GetString(result.data(), amx_str, 0, str_len);
		result.pop_back();
	}

	return result;
}

bool CAMX::SetPublicVariable(const std::string_view varname, cell value)
{
	cell var_amx_addr;
	if (FindPubVar(varname.data(), &var_amx_addr) != AMX_ERR_NONE)
		return false;

	cell* var_addr = GetAddress(var_amx_addr);
	if (var_addr == nullptr)
		return false;

	*var_addr = value;
	return true;
}

// to-do: bigger things
int amx::exec(AMX* amx, cell* retval, int index)
{
	int error = amx_Exec(amx, retval, index);

	auto op_script = mp::script_manager->Get(amx);
	if (op_script)
	{
		auto& script = op_script->second;

		if (error != AMX_ERR_NONE && error != AMX_ERR_INDEX)
		{
			const AMX_HEADER* hdr = script->AMX()->Header();
			std::string pub_name;

			if (index == AMX_EXEC_MAIN)
			{
				pub_name = "main";
			}
			else
			{
				const auto* pubfun = reinterpret_cast<const AMX_FUNCSTUBNT*>(reinterpret_cast<const unsigned char*>(hdr) + hdr->publics + (index * hdr->defsize));
				pub_name = reinterpret_cast<const char*>(reinterpret_cast<const unsigned char*>(hdr) + pubfun->nameofs);
			}

			mp::log->error("[script:{}] Failed to execute public function \"{}\".", script->Name(), pub_name);
			mp::log->error("[script:{}] Exception details:", script->Name());
			mp::log->error("[script:{}]    {}", script->Name(), CAMX::GetErrorMessage(error));
		}

		script->GC()->Collect();
	}

	return error;
}

int amx::callback_no_sysreq_d(AMX* amx, cell index, cell* result, const cell* params)
{
	auto* hdr = reinterpret_cast<AMX_HEADER*>(amx->base);

	cell entries = (hdr->libraries - hdr->natives) / hdr->defsize;
	if (index < 0 || index > entries)
		return AMX_ERR_INDEX;

	auto* func = reinterpret_cast<AMX_FUNCSTUB*>(reinterpret_cast<unsigned char*>(hdr) + static_cast<unsigned>(hdr->natives) + static_cast<unsigned>(index) * hdr->defsize);
	auto f = reinterpret_cast<AMX_NATIVE>(static_cast<uintptr_t>(func->address));

	amx->error = AMX_ERR_NONE;
	*result = f(amx, params);
	
	return amx->error;
}
