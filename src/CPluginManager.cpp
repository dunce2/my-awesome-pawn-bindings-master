#include "main.hpp"

static void logprintf(const char* format, ...)
{
	std::va_list args;
	va_start(args, format);

	static char _bull_shit[2048];
	std::vsprintf(_bull_shit, format, args);
	
	va_end(args);

	mp::log->info("{}", _bull_shit);
}

bool CPluginManager::LoadPlugin(const std::string& plugin_name)
{
	const std::filesystem::path plugin_folder_path{ std::filesystem::current_path() / "pawn" / "plugins" };
	std::filesystem::path plugin_path{ plugin_folder_path / plugin_name };
	if (!std::filesystem::exists(plugin_path))
	{
		mp::log->error("[PluginManager] Failed to load plugin \"{}\": File not found");
		return false;
	}

	plugin_path = std::filesystem::canonical(plugin_path);
	auto it = std::search(plugin_path.begin(), plugin_path.end(), plugin_folder_path.begin(), plugin_folder_path.end());
	if (it == plugin_path.begin())
	{
		mp::log->error("[PluginManager] Failed to load plugin \"{}\": Plugin is outside plugins directory (./pawn/plugins)", plugin_name);
		return false;
	}

	mp::log->info("[PluginManager] Loading plugin \"{}\".", plugin_name);

	auto plugin = std::make_unique<stPlugin>();

	if (uv_dlopen(plugin_name.c_str(), &plugin->plugin_handle) == -1)
	{
		mp::log->error("[PluginManager] Failed to load plugin: {}", std::string(uv_dlerror(&plugin->plugin_handle)));
		return false;
	}

	if (
		uv_dlsym(&plugin->plugin_handle, "Load", reinterpret_cast<void**>(&plugin->Load)) ||
		uv_dlsym(&plugin->plugin_handle, "Supports", reinterpret_cast<void**>(&plugin->Supports))
	)
	{
		mp::log->error("[PluginManager] Failed to load plugin: Plugin does not conform to architecure.");
		return false;
	}

	using namespace mp::plugins;

	unsigned supports_flags = plugin->Supports();
	if ((supports_flags & SUPPORTS_FLAGS::SUPPORTS_AMX_NATIVES) != 0)
	{
		uv_dlsym(&plugin->plugin_handle, "AmxLoad", reinterpret_cast<void**>(&plugin->AmxLoad));
		uv_dlsym(&plugin->plugin_handle, "AmxUnload", reinterpret_cast<void**>(&plugin->AmxUnload));
	}

	if ((supports_flags & SUPPORTS_FLAGS::SUPPORTS_PROCESS_TICK) != 0)
	{
		uv_dlsym(&plugin->plugin_handle, "ProcessTick", reinterpret_cast<void**>(&plugin->ProcessTick));
	}

	uv_dlsym(&plugin->plugin_handle, "Unload", reinterpret_cast<void**>(&plugin->Unload));

	if (!plugin->Load(_plugin_exports.data()))
	{
		uv_dlclose(&plugin->plugin_handle);
		return false;
	}

	_plugins[plugin_name] = std::move(plugin);

	return true;
}

void CPluginManager::UnloadPlugin(const std::string& plugin_name)
{
	if (!_plugins.contains(plugin_name))
	{
		mp::log->error("[PluginManager] Attempted to unload non-existing plugin: {}", plugin_name);
		return;
	}

	mp::log->info("[PluginManager] Unloading plugin \"{}\".", plugin_name);

	auto& plugin = _plugins[plugin_name];
	if (plugin->Unload)
	{
		try
		{
			plugin->Unload();
		}
		catch (const std::exception& e)
		{
			mp::log->debug("[PluginManager] Caught error while unloading plugin: {}", e.what());
		}
		catch (...)
		{
			mp::log->debug("[PluginManager] Caught error while unloading plugin.");
		}
	}

	uv_dlclose(&plugin->plugin_handle);

	_plugins.erase(plugin_name);

	mp::log->info("[PluginManager] Unloaded.");

}

void CPluginManager::AmxLoad(AMX* amx)
{
	for (auto&& [_, plugin] : _plugins)
	{
		if(plugin->AmxLoad)
			plugin->AmxLoad(amx);
	}
}

void CPluginManager::AmxUnload(AMX* amx)
{
	for (auto&& [_, plugin] : _plugins)
	{
		if (plugin->AmxUnload)
			plugin->AmxUnload(amx);
	}
}

void CPluginManager::ProcessTick()
{
	for (auto&& [_, plugin] : _plugins)
	{
		if (plugin->ProcessTick)
			plugin->ProcessTick();
	}
}

CPluginManager::CPluginManager()
{
	using namespace mp::plugins;

	// amx_Align64 is missing since this isn't meant to be built or used on 64-bit arch
	_amx_exports = {{
		(void*)&amx_Align16, (void*)&amx_Align32, nullptr, (void*)&amx_Allot, (void*)&amx_Callback,
		(void*)&amx_Cleanup, (void*)&amx_Clone, (void*)&amx::exec, (void*)&amx_FindNative, (void*)&amx_FindPublic,
		(void*)&amx_FindPubVar, (void*)&amx_FindTagId, (void*)&amx_Flags, (void*)&amx_GetAddr, (void*)&amx_GetNative,
		(void*)&amx_GetPublic, (void*)&amx_GetPubVar, (void*)&amx_GetString, (void*)&amx_GetTag, (void*)&amx_GetUserData,
		(void*)&amx_Init, (void*)&amx_InitJIT, (void*)&amx_MemInfo, (void*)&amx_NameLength, (void*)&amx_NativeInfo, (void*)&amx_NumNatives,
		(void*)&amx_NumPublics, (void*)&amx_NumPubVars, (void*)&amx_NumTags, (void*)&amx_Push, (void*)&amx_PushArray,
		(void*)&amx_PushString, (void*)&amx_RaiseError, (void*)&amx_Register, (void*)&amx_Release, (void*)&amx_SetCallback,
		(void*)&amx_SetDebugHook, (void*)&amx_SetString, (void*)&amx_SetUserData, (void*)&amx_StrLen,
		(void*)&amx_UTF8Check, (void*)&amx_UTF8Get, (void*)&amx_UTF8Len, (void*)&amx_UTF8Put
	}};

	_plugin_exports[PLUGIN_DATA_LOGPRINTF] = static_cast<void*>(&logprintf);
	_plugin_exports[PLUGIN_DATA_AMX_EXPORTS] = static_cast<void*>(_amx_exports.data());
	_plugin_exports[PLUGIN_DATA_NETGAME] = nullptr;
	_plugin_exports[PLUGIN_DATA_CONSOLE] = nullptr;
	_plugin_exports[PLUGIN_DATA_RAKSERVER] = nullptr;
	_plugin_exports[PLUGIN_DATA_LOADFSCRIPT] = nullptr;
	_plugin_exports[PLUGIN_DATA_UNLOADFSCRIPT] = nullptr;
}