#pragma once

class CScript;

class CTimer 
{
	uv_timer_t _timer_handle;
	int _callback_idx{ -1 };
	std::shared_ptr<CScript> _script;
	std::vector<CScript::ParamType> _params;
	bool _old_timer{ false };
	bool _killed{ false };
	int _paused_time{ 0 };
	unsigned _timeout_time{ 0U };
	unsigned _repeat_time{ 0U };

	static void Callback(uv_timer_t* handle);

public:
	CTimer(uv_loop_t* loop, const std::shared_ptr<CScript>& script, bool is_old = false);
	CTimer(const CTimer&) = delete;
	CTimer(CTimer&&) = delete;
	~CTimer();

	IO_GETTER_SETTER(Timeout, _timeout_time)
	IO_GETTER_SETTER(Interval, _repeat_time)
	IO_GETTER_SETTER(Legacy, _old_timer)
	IO_GETTER_SETTER(Script, _script)
	IO_GETTER_SETTER(Callback, _callback_idx)
	IO_GETTER_SETTER(Params, _params)
	IO_GETTER_SETTER(Killed, _killed)
	inline std::uint64_t DueIn() const { return uv_timer_get_due_in(&_timer_handle); }
	inline void SetInterval(std::uint64_t repeat) { uv_timer_set_repeat(&_timer_handle, (_repeat_time = repeat)); }
	bool SetCallback(const cell* params, int callback_idx, int format_idx = -1, int params_idx = -1);

	void Run();
	void Stop();

};