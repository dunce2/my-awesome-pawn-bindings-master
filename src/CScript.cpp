#include "main.hpp"
#include "CScript.hpp"

void CScript::Cleanup()
{
	_timers->DeleteAll();
}

void CScript::ExtractParams(const cell* params, int format_idx, int params_idx, std::vector<ParamType>& destination, bool reverse)
{
	unsigned param_count = params[0] / sizeof(cell);

	if (param_count < format_idx)
		return;

	std::string format = _amx->GetString(_amx->GetAddress(params[format_idx]));
	std::size_t format_len = format.length();
	if (format_len > param_count - format_idx)
		return;

	for (size_t j = 0U; j < format_len; ++j)
	{
		switch (format[j])
		{
			case 'd':
			case 'i':
			case 'c':
			case 'x':
			case 'f':
			{
				destination.push_back(*_amx->GetAddress(params[params_idx++]));
				break;
			}
			case 'a':
			{
				if (j + 1 >= format_len || (format[j + 1] != 'd' || format[j + 1] != 'i'))
					break;

				cell* array = _amx->GetAddress(params[params_idx++]);
				cell size = *_amx->GetAddress(params[params_idx++]);
				std::vector<cell> array_vec(&array[0], &array[size]);
				destination.push_back(std::move(array_vec));
				j++;

				break;
			}
			case 's':
			{
				cell* string_ptr = _amx->GetAddress(params[params_idx++]);
				int str_len{};
				amx_StrLen(string_ptr, &str_len);
				
				std::vector<cell> array_vec(&string_ptr[0], &string_ptr[str_len + 1]);
				destination.push_back(std::move(array_vec));

				break;
			}
		}
	}

	if (reverse)
		std::reverse(destination.begin(), destination.end());
}

void CScript::PushParamVector(const std::vector<ParamType>& values)
{
	for (auto&& param : values)
	{
		if (std::holds_alternative<cell>(param))
		{
			_amx->Push(std::get<cell>(param));
		}
		else
		{
			const std::vector<cell>& array = std::get<std::vector<cell>>(param);
			_amx->PushArray(nullptr, array.data(), array.size());
		}
	}
}
