#pragma once

class CScript;
class CTimer;

class CTimerManager
{
	unsigned _highest_timer_id{ 0U };
	std::unordered_map<unsigned, std::unique_ptr<CTimer>> _timers;

public:
	~CTimerManager();

	std::pair<unsigned, CTimer*> New(std::shared_ptr<CScript> script, bool old = false);
	CTimer* Get(unsigned id);
	void Delete(unsigned id);
	void DeleteAll();

	void FreeKilled();
};