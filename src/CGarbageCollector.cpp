#include "main.hpp"

CGarbageCollector::~CGarbageCollector()
{
	for (auto&& [ptr, sym] : _collection)
	{
		sym.destructor(ptr);
	}

	_collection.clear();
}

void CGarbageCollector::Collect()
{
	size_t count = std::erase_if(_collection, [](const std::pair<void*, symbol>& v) {
		if (v.second.ref_count <= 0)
		{
			v.second.destructor(v.first);
			return true;
		}
		return false;
	});

	mp::log->debug("[gc] Collected {} symbols from the garbage collector.", count);
}
