#include "main.hpp"

void CMongPawnServer::OnLoad()
{
	/*
	auto filesink = std::make_shared<spdlog::sinks::daily_file_sink_mt>("pawn/logs/daily.log", 23, 59);
	auto colorsink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	// spdlog::init_thread_pool(8192, 1);
	mp::log = std::make_shared<spdlog::logger>("mongpawn", spdlog::sinks_init_list{ filesink, colorsink });
	spdlog::register_logger(mp::log);
	mp::log->set_pattern("[%T] [t:%t] [%^%L%$] %v");
	*/
	mp::log = rm::logger;
	mp::log->set_pattern("%^[%H:%M:%S] [t:%t]%$ %v");

	std::filesystem::path plugin_path{ std::filesystem::current_path() / "pawn" };
	std::filesystem::create_directories(plugin_path / "scriptfiles");
	std::filesystem::create_directory(plugin_path / "gamemodes");
	std::filesystem::create_directory(plugin_path / "filterscripts");
	std::filesystem::create_directory(plugin_path / "plugins");

	if (!mp::config::load_and_parse())
		return;

#ifdef _WIN32
	SetEnvironmentVariable("AMXFILE", (plugin_path / "scriptfiles").string().c_str());
#else
	setenv("AMXFILE", scriptfiles_path.string().c_str(), 1);
#endif

	if (Botan::Processor_RNG::available())
		mp::rng = std::make_unique<Botan::Processor_RNG>();
	else
		mp::rng = std::make_unique<Botan::System_RNG>();

	uv_async_init(uv_default_loop(), &mp::async_stop_event_loop, [](uv_async_t* h) -> void {
		uv_stop(h->loop);
	});

	uv_async_init(uv_default_loop(), &mp::async_process_task_queue, [](uv_async_t* /*h*/) -> void {
		while (!mp::evloop_task_queue->empty())
		{
			auto f = mp::evloop_task_queue->front();
			mp::evloop_task_queue->pop();
			f();
		}
	});

	uv_async_init(uv_default_loop(), &mp::async_timer_collection, [](uv_async_t* /*h*/) -> void {
		mp::script_manager->ForEach([](const auto& s) {
			s.second->Timers()->FreeKilled();
		});
	});

	mp::log->info("= - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - =");
	mp::log->info("={:^79}=", R"(   ____ ___  ____  ____  ____ _      ____  ____ __      ______ )");
	mp::log->info("={:^79}=", R"(  / __ `__ \/ __ \/ __ \/ __ `/_____/ __ \/ __ `/ | /| / / __ \)");
	mp::log->info("={:^79}=", R"( / / / / / / /_/ / / / / /_/ /_____/ /_/ / /_/ /| |/ |/ / / / /)");
	mp::log->info("={:^79}=", R"(/_/ /_/ /_/\____/_/ /_/\__, /     / .___/\__,_/ |__/|__/_/ /_/ )");
	mp::log->info("={:^79}=", R"(                      /____/     /_/                           )");
	mp::log->info("={:^79s}=", (CUR_FILE_VERSION == 9 ? "3.2" : "4.0"));
	mp::log->info("={:^79c}=", ' ');
	mp::log->info("={:^79}=", R"(_______________________________)");
	mp::log->info("={:^79}=", fmt::format(R"(__/  version: 0.0.1 | jit: {:5}  \__)", mp::config::features.test(mp::config::config_features::jit_compilation)));
	mp::log->info("= - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - =");

	mp::log->debug("Registering console variables...");

	const auto vector_join = [](const std::vector<std::string>& v) -> std::string
	{
		if (v.empty())
			return std::string();

		std::string res;
		for(auto&& s : v)
		{
			res += s + ' ';
		}
		res.pop_back();
		return std::move(res);
	};

	mp::console->SetVariable("announce", true);
	mp::console->SetVariable("rcon", true);
	mp::console->SetVariable("maxplayers", PLAYER_POOL_SIZE, CConsole::var_flags::read_only);
	mp::console->SetVariable("port", 4220, CConsole::var_flags::read_only);
	mp::console->SetVariable("bind", "0.0.0.0", CConsole::var_flags::read_only);
	mp::console->SetVariable("lanmode", false);
	mp::console->SetVariable("query", true);
	mp::console->SetVariable("logqueries", false);
	mp::console->SetVariable("output", false);
	mp::console->SetVariable("mtu", 576, CConsole::var_flags::read_only);
	mp::console->SetVariable("timestamp", true);
	mp::console->SetVariable("logtimeformat", "[%H:%M:%S]");
	mp::console->SetVariable("password", "");
	mp::console->SetVariable("hostname", "open.mp Server");
	mp::console->SetVariable("mapname", "San Andreas", CConsole::var_flags::rule);
	mp::console->SetVariable("language", "", CConsole::var_flags::rule);
	mp::console->SetVariable("weburl", "rakmong.com", CConsole::var_flags::rule);
	mp::console->SetVariable("rcon_password", "changeme");
	mp::console->SetVariable("gravity", "0.008", CConsole::var_flags::rule);
	mp::console->SetVariable("weather", "10", CConsole::var_flags::rule);
	mp::console->SetVariable("tirepopping", "0", CConsole::var_flags::rule);
	mp::console->SetVariable("gamemodetext", "Unknown");
	mp::console->SetVariable("filterscripts", vector_join(mp::config::filterscripts), CConsole::var_flags::read_only);
	mp::console->SetVariable("plugins", vector_join(mp::config::plugins), CConsole::var_flags::read_only);
	mp::console->SetVariable("nosign", "");
	mp::console->SetVariable("anticheat", false, CConsole::var_flags::rule);
	mp::console->SetVariable("myriad", false);
	mp::console->SetVariable("chatlogging", 1);
	mp::console->SetVariable("messageholelimit", 3000);
	mp::console->SetVariable("playertimeout", 10000);
	mp::console->SetVariable("db_logging", 0);
	mp::console->SetVariable("db_log_queries", 0);
	mp::console->SetVariable("onfoot_rate", 30);
	mp::console->SetVariable("incar_rate", 30);
	mp::console->SetVariable("weapon_rate", 30);
	mp::console->SetVariable("stream_rate", 1000);
	mp::console->SetVariable("stream_distance", 200.F);
	mp::console->SetVariable("sleep", 5);
	mp::console->SetVariable("connseedtime", 300000);
	mp::console->SetVariable("conncookies", 1);
	mp::console->SetVariable("cookielogging", 0);
	mp::console->SetVariable("minconnectiontime", 0);
	mp::console->SetVariable("messageslimit", 500);
	mp::console->SetVariable("ackslimit", 3000);

	for (size_t i{ 0U }; auto&& gm : mp::config::gamemodes)
	{
		mp::console->SetVariable(fmt::format("gamemode{}", i), gm);
		i++;
	}

	mp::log->debug("Registering native functions...");
	mp::natives::RegisterAllNatives();

	if (mp::config::gamemodes.empty())
	{
		mp::log->critical("You didn't provide a gamemode in the config file (located at \"{}\").", mp::config::config_file_path);
		return;
	}

	if (!mp::config::filterscripts.empty())
	{
		mp::log->info("Loading filterscripts...");
		mp::log->info("= - - - - - - - - - - - - - - - - - - -");
		mp::log->info(" ");
		for (auto&& filterscript : mp::config::filterscripts)
		{
			mp::script_manager->Load(filterscript, true);
			mp::log->info(" ");
		}
		mp::log->info("= - - - - - - - - - - - - - - - - - - -\n");
	}

	if (mp::script_manager->Load(mp::config::gamemodes[0], false))
	{
		mp::log->critical("Failed to load first gamemode.");
		mp::script_manager.reset();
		mp::log->flush();
		return;
	}

	mp::log->debug("Spawning event loop thread...");
	mp::event_loop_thread = std::thread([](uv_loop_t* loop) {
		mp::log->debug("Starting event loop...");
		uv_run(loop, UV_RUN_DEFAULT);
		mp::log->debug("Event loop closed.");
	}, uv_default_loop());
}

void CMongPawnServer::OnUpdate()
{
	if (uv_is_active(reinterpret_cast<uv_handle_t*>(&mp::async_timer_collection)))
	{
		uv_async_send(&mp::async_timer_collection);
	}

	if (mp::exited_gamemode)
	{
		mp::exited_gamemode = false;

		mp::script_manager->Unload(CScriptManager::gamemode_id);

		mp::current_gamemode++;
		if (mp::current_gamemode >= mp::config::gamemodes.size())
			mp::current_gamemode = 0U;

		if (mp::script_manager->Load(mp::config::gamemodes[mp::current_gamemode], false))
		{
			mp::log->critical("Failed to load gamemode.");
			Unload();
			return;
		}
	}
}

void CMongPawnServer::OnUnload()
{
	uv_async_send(&mp::async_stop_event_loop);
	mp::event_loop_thread.join();

	mp::script_manager->UnloadAll();

	uv_loop_t* loop = uv_default_loop();
	if (uv_loop_close(loop) == UV_EBUSY)
	{
		uv_walk(loop, [](uv_handle_t* h, void* arg) -> void {
			// Weird bug I got when calling uv_walk with recently-closed handles. Valid handle pointer but had all fields zeroed.
			if (!h || !h->loop)
				return;

			if (uv_is_closing(h))
				return;

			mp::log->debug("[evloop:{}] Closing handle {}", uv_handle_type_name(uv_handle_get_type(h)), fmt::ptr(h));
			uv_close(h, [](uv_handle_t* h) {
				mp::log->debug("[evloop] Closed {}.", fmt::ptr(h));
			});			
		}, nullptr);

		uv_run(loop, UV_RUN_DEFAULT);
		uv_loop_close(loop);
	}

	mp::log->flush();
}

bool CMongPawnServer::OnPlayerConnect(rm::player* player)
{
	mp::evloop_task_queue->push([=]() {
		mp::script_manager->Call<CScriptManager::call_mode::call_filterscripts_first, 0>("OnPlayerConnect", player->Id());
	});
	uv_async_send(&mp::async_process_task_queue);

	return true;
}
