#include "CTimerManager.hpp"
#include "CTimerManager.hpp"
#include "CTimerManager.hpp"
#include "main.hpp"

CTimerManager::~CTimerManager()
{
	_timers.clear();
}

std::pair<unsigned, CTimer*> CTimerManager::New(std::shared_ptr<CScript> script, bool old)
{
	auto it = _timers.insert({ ++_highest_timer_id, std::make_unique<CTimer>(uv_default_loop(), script, old) });
	if (!it.second)
	{
		--_highest_timer_id;
		return { 0U, nullptr };
	}

	return { it.first->first, it.first->second.get() };
}

CTimer* CTimerManager::Get(unsigned id)
{
	auto it = _timers.find(id);

	if (it == _timers.end())
		return nullptr;

	return it->second.get();
}

void CTimerManager::Delete(unsigned id)
{
	_timers.erase(id);
}

void CTimerManager::DeleteAll()
{
	_timers.clear();
}

void CTimerManager::FreeKilled()
{
	std::erase_if(_timers, [](const auto& v) {
		return v.second->Killed();
	});
}
