#pragma once

#define IO_GETTER_SETTER(func,prop) \
	auto& func() { return prop; }\
	const auto& func() const { return prop; }

namespace mp::utils {
	void replace_chars(std::string& string, const std::string_view find, const std::string_view replace);
	void call_evloop_sync(const std::function<void()>& fun);

	constexpr std::size_t hash_string(const std::string_view str)
	{
		constexpr std::size_t fnv_prime = 16777619U;
		constexpr std::size_t fnv_offset_basis = 2166136261U;
		std::size_t hash = fnv_offset_basis;

		for (std::size_t i = 0; i < str.length(); ++i)
		{
			hash ^= str[i];
			hash *= fnv_prime;
		}

		return hash;
	}
}

constexpr unsigned operator"" _hash(const char* l, size_t)
{
	return mp::utils::hash_string(l);
}