#pragma once

extern "C" {
	int amx_CoreInit(AMX* amx);
	int amx_CoreCleanup(AMX* amx);
	int amx_FloatInit(AMX* amx);
	int amx_FloatCleanup(AMX* amx);
	int amx_StringInit(AMX* amx);
	int amx_StringCleanup(AMX* amx);
	int amx_TimeInit(AMX* amx);
	int amx_TimeCleanup(AMX* amx);
	int amx_FileInit(AMX* amx);
	int amx_FileCleanup(AMX* amx);

#ifdef MP_AMX4
	int amx_exec_list(AMX* amx, const cell** opcodelist, int* numopcodes);

	#if __GNUC__
		extern int amx_jit_list(const AMX* amx, const cell** opcodelist, int* numopcodes) __attribute__((cdecl));
	#elif _WIN32
		extern int __cdecl amx_jit_list(const AMX* amx, const cell** opcodelist, int* numopcodes);
	#endif
#else
	extern int AMXAPI getMaxCodeSize();
#endif
};

namespace amx {
	extern "C" int exec(AMX* amx, cell * retval, int index);
	extern "C" int callback_no_sysreq_d(AMX* amx, cell index, cell* result, const cell* params);

	template<class T>
#if defined __GNUC__
	constexpr
#endif
	inline auto swapbytes(T v) -> std::enable_if_t<std::is_integral_v<T>, T>
	{
		static_assert(sizeof(T) >= 2 && sizeof(T) <= 8, "can't swap bytes from types that aren't at least 2 bytes or are bigger than 8 bytes");
		static_assert(sizeof(T) % 2 == 0);

#ifdef _MSC_VER
		if constexpr (sizeof(T) == 2)
			return _byteswap_ushort(v);
		else if constexpr (sizeof(T) == 4)
			return _byteswap_ulong(v);
		else if constexpr (sizeof(T) == 8)
			return _byteswap_uint64(v);
#elif __has_builtin(__builtin_bswap32)
		if constexpr (sizeof(T) == 2)
			return __builtin_bswap16(v);
		else if constexpr (sizeof(T) == 4)
			return __builtin_bswap32(v);
		else if constexpr (sizeof(T) == 8)
			return __builtin_bswap64(v);
#endif

		return v;
	}

	template<class To, class From>
	inline To cast(From v)
	{
		return *reinterpret_cast<To*>(&v);
	}

#pragma region opcodes
	struct opcode {
		cell num_params;
		bool needs_relocation;
	};

	enum opcode_list : ucell {
		OP_NONE,
		OP_LOAD_PRI,
		OP_LOAD_ALT,
		OP_LOAD_S_PRI,
		OP_LOAD_S_ALT,
		OP_LREF_PRI,
		OP_LREF_ALT,
		OP_LREF_S_PRI,
		OP_LREF_S_ALT,
		OP_LOAD_I,
		OP_LODB_I,
		OP_CONST_PRI,
		OP_CONST_ALT,
		OP_ADDR_PRI,
		OP_ADDR_ALT,
		OP_STOR_PRI,
		OP_STOR_ALT,
		OP_STOR_S_PRI,
		OP_STOR_S_ALT,
		OP_SREF_PRI,
		OP_SREF_ALT,
		OP_SREF_S_PRI,
		OP_SREF_S_ALT,
		OP_STOR_I,
		OP_STRB_I,
		OP_LIDX,
		OP_LIDX_B,
		OP_IDXADDR,
		OP_IDXADDR_B,
		OP_ALIGN_PRI,
		OP_ALIGN_ALT,
		OP_LCTRL,
		OP_SCTRL,
		OP_MOVE_PRI,
		OP_MOVE_ALT,
		OP_XCHG,
		OP_PUSH_PRI,
		OP_PUSH_ALT,
		OP_PUSH_R,
		OP_PUSH_C,
		OP_PUSH,
		OP_PUSH_S,
		OP_POP_PRI,
		OP_POP_ALT,
		OP_STACK,
		OP_HEAP,
		OP_PROC,
		OP_RET,
		OP_RETN,
		OP_CALL,
		OP_CALL_PRI,
		OP_JUMP,
		OP_JREL,
		OP_JZER,
		OP_JNZ,
		OP_JEQ,
		OP_JNEQ,
		OP_JLESS,
		OP_JLEQ,
		OP_JGRTR,
		OP_JGEQ,
		OP_JSLESS,
		OP_JSLEQ,
		OP_JSGRTR,
		OP_JSGEQ,
		OP_SHL,
		OP_SHR,
		OP_SSHR,
		OP_SHL_C_PRI,
		OP_SHL_C_ALT,
		OP_SHR_C_PRI,
		OP_SHR_C_ALT,
		OP_SMUL,
		OP_SDIV,
		OP_SDIV_ALT,
		OP_UMUL,
		OP_UDIV,
		OP_UDIV_ALT,
		OP_ADD,
		OP_SUB,
		OP_SUB_ALT,
		OP_AND,
		OP_OR,
		OP_XOR,
		OP_NOT,
		OP_NEG,
		OP_INVERT,
		OP_ADD_C,
		OP_SMUL_C,
		OP_ZERO_PRI,
		OP_ZERO_ALT,
		OP_ZERO,
		OP_ZERO_S,
		OP_SIGN_PRI,
		OP_SIGN_ALT,
		OP_EQ,
		OP_NEQ,
		OP_LESS,
		OP_LEQ,
		OP_GRTR,
		OP_GEQ,
		OP_SLESS,
		OP_SLEQ,
		OP_SGRTR,
		OP_SGEQ,
		OP_EQ_C_PRI,
		OP_EQ_C_ALT,
		OP_INC_PRI,
		OP_INC_ALT,
		OP_INC,
		OP_INC_S,
		OP_INC_I,
		OP_DEC_PRI,
		OP_DEC_ALT,
		OP_DEC,
		OP_DEC_S,
		OP_DEC_I,
		OP_MOVS,
		OP_CMPS,
		OP_FILL,
		OP_HALT,
		OP_BOUNDS,
		OP_SYSREQ_PRI,
		OP_SYSREQ_C,
		OP_FILE,
		OP_LINE,
		OP_SYMBOL,
		OP_SRANGE,
		OP_JUMP_PRI,
		OP_SWITCH,
		OP_CASETBL,
		OP_SWAP_PRI,
		OP_SWAP_ALT,
		OP_PUSH_ADR,
		OP_NOP,
		OP_SYSREQ_N,
		OP_SYMTAG,
		OP_BREAK,
		OP_PUSH2_C,
		OP_PUSH2,
		OP_PUSH2_S,
		OP_PUSH2_ADR,
		OP_PUSH3_C,
		OP_PUSH3,
		OP_PUSH3_S,
		OP_PUSH3_ADR,
		OP_PUSH4_C,
		OP_PUSH4,
		OP_PUSH4_S,
		OP_PUSH4_ADR,
		OP_PUSH5_C,
		OP_PUSH5,
		OP_PUSH5_S,
		OP_PUSH5_ADR,
		OP_LOAD_BOTH,
		OP_LOAD_S_BOTH,
		OP_CONST,
		OP_CONST_S,
		/* ----- */
		OP_SYSREQ_D,
		OP_SYSREQ_ND,

		OP_NUM_OPCODES
	};

	inline constexpr std::array<opcode, OP_NUM_OPCODES> opcodes = {{
		{ 0, false },		// OP_NONE
		{ 1, false },		// OP_LOAD_PRI
		{ 1, false },		// OP_LOAD_ALT
		{ 1, false },		// OP_LOAD_S_PRI
		{ 1, false },		// OP_LOAD_S_ALT
		{ 1, false },		// OP_LREF_PRI
		{ 1, false },		// OP_LREF_ALT
		{ 1, false },		// OP_LREF_S_PRI
		{ 1, false },		// OP_LREF_S_ALT
		{ 0, false },		// OP_LOAD_I
		{ 1, false },		// OP_LODB_I
		{ 1, false },		// OP_CONST_PRI
		{ 1, false },		// OP_CONST_ALT
		{ 1, false },		// OP_ADDR_PRI
		{ 1, false },		// OP_ADDR_ALT
		{ 1, false },		// OP_STOR_PRI
		{ 1, false },		// OP_STOR_ALT
		{ 1, false },		// OP_STOR_S_PRI
		{ 1, false },		// OP_STOR_S_ALT
		{ 1, false },		// OP_SREF_PRI
		{ 1, false },		// OP_SREF_ALT
		{ 1, false },		// OP_SREF_S_PRI
		{ 1, false },		// OP_SREF_S_ALT
		{ 0, false },		// OP_STOR_I
		{ 1, false },		// OP_STRB_I
		{ 0, false },		// OP_LIDX
		{ 1, false },		// OP_LIDX_B
		{ 0, false },		// OP_IDXADDR
		{ 1, false },		// OP_IDXADDR_B
		{ 1, false },		// OP_ALIGN_PRI
		{ 1, false },		// OP_ALIGN_ALT
		{ 1, false },		// OP_LCTRL
		{ 1, false },		// OP_SCTRL
		{ 0, false },		// OP_MOVE_PRI
		{ 0, false },		// OP_MOVE_ALT
		{ 0, false },		// OP_XCHG
		{ 0, false },		// OP_PUSH_PRI
		{ 0, false },		// OP_PUSH_ALT
		{ 1, false },		// OP_PUSH_R
		{ 1, false },		// OP_PUSH_C
		{ 1, false },		// OP_PUSH
		{ 1, false },		// OP_PUSH_S
		{ 0, false },		// OP_POP_PRI
		{ 0, false },		// OP_POP_ALT
		{ 1, false },		// OP_STACK
		{ 1, false },		// OP_HEAP
		{ 0, false },		// OP_PROC
		{ 0, false },		// OP_RET
		{ 0, false },		// OP_RETN
		{ 1, true },			// OP_CALL
		{ 0, false },		// OP_CALL_PRI
		{ 1, true },			// OP_JUMP
		{ 1, true },			// OP_JREL
		{ 1, true },			// OP_JZER
		{ 1, true },			// OP_JNZ
		{ 1, true },			// OP_JEQ
		{ 1, true },			// OP_JNEQ
		{ 1, true },			// OP_JLESS
		{ 1, true },			// OP_JLEQ
		{ 1, true },			// OP_JGRTR
		{ 1, true },			// OP_JGEQ
		{ 1, true },			// OP_JSLESS
		{ 1, true },			// OP_JSLEQ
		{ 1, true },			// OP_JSGRTR
		{ 1, true },			// OP_JSGEQ
		{ 0, false },		// OP_SHL
		{ 0, false },		// OP_SHR
		{ 0, false },		// OP_SSHR
		{ 1, false },		// OP_SHL_C_PRI
		{ 1, false },		// OP_SHL_C_ALT
		{ 1, false },		// OP_SHR_C_PRI
		{ 1, false },		// OP_SHR_C_ALT
		{ 0, false },		// OP_SMUL
		{ 0, false },		// OP_SDIV
		{ 0, false },		// OP_SDIV_ALT
		{ 0, false },		// OP_UMUL
		{ 0, false },		// OP_UDIV
		{ 0, false },		// OP_UDIV_ALT
		{ 0, false },		// OP_ADD
		{ 0, false },		// OP_SUB
		{ 0, false },		// OP_SUB_ALT
		{ 0, false },		// OP_AND
		{ 0, false },		// OP_OR
		{ 0, false },		// OP_XOR
		{ 0, false },		// OP_NOT
		{ 0, false },		// OP_NEG
		{ 0, false },		// OP_INVERT
		{ 1, false },		// OP_ADD_C
		{ 1, false },		// OP_SMUL_C
		{ 0, false },		// OP_ZERO_PRI
		{ 0, false },		// OP_ZERO_ALT
		{ 1, false },		// OP_ZERO
		{ 1, false },		// OP_ZERO_S
		{ 0, false },		// OP_SIGN_PRI
		{ 0, false },		// OP_SIGN_ALT
		{ 0, false },		// OP_EQ
		{ 0, false },		// OP_NEQ
		{ 0, false },		// OP_LESS
		{ 0, false },		// OP_LEQ
		{ 0, false },		// OP_GRTR
		{ 0, false },		// OP_GEQ
		{ 0, false },		// OP_SLESS
		{ 0, false },		// OP_SLEQ
		{ 0, false },		// OP_SGRTR
		{ 0, false },		// OP_SGEQ
		{ 1, false },		// OP_EQ_C_PRI
		{ 1, false },		// OP_EQ_C_ALT
		{ 0, false },		// OP_INC_PRI
		{ 0, false },		// OP_INC_ALT
		{ 1, false },		// OP_INC
		{ 1, false },		// OP_INC_S
		{ 0, false },		// OP_INC_I
		{ 0, false },		// OP_DEC_PRI
		{ 0, false },		// OP_DEC_ALT
		{ 1, false },		// OP_DEC
		{ 1, false },		// OP_DEC_S
		{ 0, false },		// OP_DEC_I
		{ 1, false },		// OP_MOVS
		{ 1, false },		// OP_CMPS
		{ 1, false },		// OP_FILL
		{ 1, false },		// OP_HALT
		{ 1, false },		// OP_BOUNDS
		{ 0, false },		// OP_SYSREQ_PRI
		{ 1, false },		// OP_SYSREQ_C
		{ INT_MIN, false }, // OP_FILE
		{ 2, false },		// OP_LINE
		{ INT_MIN, false }, // OP_SYMBOL
		{ 2, false },		// OP_SRANGE
		{ 0, false },		// OP_JUMP_PRI
		{ 1, true },			// OP_SWITCH
		{ INT_MAX, false }, // OP_CASETBL
		{ 0, false },		// OP_SWAP_PRI
		{ 0, false },		// OP_SWAP_ALT
		{ 1, false },		// OP_PUSH_ADR
		{ 0, false },		// OP_NOP
		{ 2, false },		// OP_SYSREQ_N
		{ 1, false },		// OP_SYMTAG
		{ 0, false },		// OP_BREAK
		{ 2, false },		// OP_PUSH2_C
		{ 2, false },		// OP_PUSH2
		{ 2, false },		// OP_PUSH2_S
		{ 2, false },		// OP_PUSH2_ADR
		{ 3, false },		// OP_PUSH3_C
		{ 3, false },		// OP_PUSH3
		{ 3, false },		// OP_PUSH3_S
		{ 3, false },		// OP_PUSH3_ADR
		{ 4, false },		// OP_PUSH4_C
		{ 4, false },		// OP_PUSH4
		{ 4, false },		// OP_PUSH4_S
		{ 4, false },		// OP_PUSH4_ADR
		{ 5, false },		// OP_PUSH5_C
		{ 5, false },		// OP_PUSH5
		{ 5, false },		// OP_PUSH5_S
		{ 5, false },		// OP_PUSH5_ADR
		{ 2, false },		// OP_LOAD_BOTH
		{ 2, false },		// OP_LOAD_S_BOTH
		{ 2, false },		// OP_CONST
		{ 2, false },		// OP_CONST_S
		{ 1, false },		// OP_SYSREQ_D
		{ 2, false }			// OP_SYSREQ_ND
	}};

#pragma endregion
}

class CAMX {
	std::unique_ptr<AMX> _amx;
	int _main_address{ -1 };

	static constexpr const char* _error_messages[] = {
		/* AMX_ERR_NONE      */ "(none)",
		/* AMX_ERR_EXIT      */ "Forced exit",
		/* AMX_ERR_ASSERT    */ "Assertion failed",
		/* AMX_ERR_STACKERR  */ "Stack/heap collision (insufficient stack size)",
		/* AMX_ERR_BOUNDS    */ "Array index out of bounds",
		/* AMX_ERR_MEMACCESS */ "Invalid memory access",
		/* AMX_ERR_INVINSTR  */ "Invalid instruction",
		/* AMX_ERR_STACKLOW  */ "Stack underflow",
		/* AMX_ERR_HEAPLOW   */ "Heap underflow",
		/* AMX_ERR_CALLBACK  */ "No (valid) native function callback",
		/* AMX_ERR_NATIVE    */ "Native function failed",
		/* AMX_ERR_DIVIDE    */ "Divide by zero",
		/* AMX_ERR_SLEEP     */ "(sleep mode)",
		/* 13 */                "(reserved)",
		/* 14 */                "(reserved)",
		/* 15 */                "(reserved)",
		/* AMX_ERR_MEMORY    */ "Out of memory",
		/* AMX_ERR_FORMAT    */ "Invalid/unsupported P-code file format",
		/* AMX_ERR_VERSION   */ "File is for a newer version of the AMX",
		/* AMX_ERR_NOTFOUND  */ "File or function is not found",
		/* AMX_ERR_INDEX     */ "Invalid index parameter (bad entry point)",
		/* AMX_ERR_DEBUG     */ "Debugger cannot run",
		/* AMX_ERR_INIT      */ "AMX not initialized (or doubly initialized)",
		/* AMX_ERR_USERDATA  */ "Unable to set user data field (table full)",
		/* AMX_ERR_INIT_JIT  */ "Cannot initialize the JIT",
		/* AMX_ERR_PARAMS    */ "Parameter error",
		/* AMX_ERR_DOMAIN    */ "Domain error, expression result does not fit in range",
		/* AMX_ERR_GENERAL   */ "General error (unknown or unspecific error)",
	};

	int Relocate();

public:
	static constexpr const char* GetErrorMessage(int error)
	{
		return _error_messages[error];
	}

	explicit CAMX(const std::filesystem::path pcode_path);
	~CAMX();

	CAMX(CAMX&&) = delete;
	CAMX(const CAMX&) = delete;

	int Compile();

	inline AMX* RawAMX() { return _amx.get(); }
	inline const AMX* RawAMX() const { return _amx.get(); }
	inline AMX_HEADER* Header() { return reinterpret_cast<AMX_HEADER*>(_amx->base); }
	inline unsigned char* Data() { return (_amx->data ? _amx->data : _amx->base + reinterpret_cast<AMX_HEADER*>(_amx->base)->dat); }
	IO_GETTER_SETTER(STK, _amx->stk);
	IO_GETTER_SETTER(HEA, _amx->hea);
	IO_GETTER_SETTER(CIP, _amx->cip);
	IO_GETTER_SETTER(FRM, _amx->frm);
	IO_GETTER_SETTER(PRI, _amx->pri);
	IO_GETTER_SETTER(ALT, _amx->alt);
	IO_GETTER_SETTER(Flags, _amx->flags);

#pragma region Wrappers
	inline int Allot(unsigned cells, cell* amx_addr, cell** phys_addr)
	{
#ifdef MP_AMX4
		auto err = amx_Allot(_amx.get(), cells, phys_addr);
		*amx_addr = reinterpret_cast<unsigned char*>(phys_addr) - Data();
		return err;
#else
		return amx_Allot(_amx.get(), cells, amx_addr, phys_addr);
#endif
	}

	inline int Allot(unsigned cells, cell** phys_addr)
	{
#ifdef MP_AMX4
		return amx_Allot(_amx.get(), cells, phys_addr);
#else
		return amx_Allot(_amx.get(), cells, nullptr, phys_addr);
#endif
	}

	inline int Callback(cell index, cell* result, const cell* params) { return amx_Callback(_amx.get(), index, result, params); }
	inline int Cleanup() { return amx_Cleanup(_amx.get()); }
	inline int Clone(AMX* clone, void* data) { return amx_Clone(_amx.get(), clone, data); }
	inline int Exec(cell* retval, int index) { 
		if (index == AMX_EXEC_MAIN)
			Header()->cip = _main_address;
		return amx::exec(_amx.get(), retval, index); 
	}
	inline int FindNative(const char* name, int* index) { return amx_FindNative(_amx.get(), name, index); }
	inline int FindPublic(const char* name, int* index) { return amx_FindPublic(_amx.get(), name, index); }
#ifdef MP_AMX4
	inline int FindPubVar(const char* name, cell** address) { return amx_FindPubVar(_amx.get(), name, address); }
#else
	int FindPubVar(const char* name, cell** address) {
		*address = nullptr;

		int i{ 0 };
		NameLength(&i);
		char* varname = new char[i];
		cell amx_address{ 0 };

		amx_NumPubVars(_amx.get(), &i);
		while (i-- > -1)
		{
			if (amx_GetPubVar(_amx.get(), i, varname, &amx_address) == AMX_ERR_NONE && !std::strcmp(varname, name))
				*address = GetAddress(amx_address);
		}

		delete[] varname;

		if (*address == nullptr)
			return AMX_ERR_INDEX;

		return AMX_ERR_NONE;
	}
#endif

	inline int FindPubVar(const char* name, cell* amx_address) {
#ifdef MP_AMX4
		auto err = amx_FindPubVar(_amx.get(), name, &amx_address);
		if (err == AMX_ERR_NONE)
		{
			*amx_address = *reinterpret_cast<cell*>(amx_address - reinterpret_cast<unsigned>(Data()));
		}

		return err;
#else
		return amx_FindPubVar(_amx.get(), name, amx_address);
#endif
	}

	inline int FindTagId(cell tag_id, char* tagname) { return amx_FindTagId(_amx.get(), tag_id, tagname); }
	inline int Flags(std::uint16_t* flags) { return amx_Flags(_amx.get(), flags); }
	inline int GetNative(int index, char* name) { return amx_GetNative(_amx.get(), index, name); }
	inline int GetPublic(int index, char* name, ucell* address) {
#ifdef MP_AMX4
		return amx_GetPublic(_amx.get(), index, name, address);
#else
		AMX_HEADER* hdr = Header();

		if (index < 0 || (hdr->natives - hdr->publics) / hdr->defsize < index)
			return AMX_ERR_INDEX;

		AMX_FUNCSTUBNT* func = reinterpret_cast<AMX_FUNCSTUBNT*>(hdr + hdr->publics + index * hdr->defsize);
		if (func)
		{
			*address = func->address;
			std::strcpy(name, reinterpret_cast<const char*>(reinterpret_cast<unsigned char*>(hdr) + func->nameofs));
		}

		return AMX_ERR_NONE;
#endif
	}
	inline int GetPublic(int index, char* name) {
#ifdef MP_AMX4
		return amx_GetPublic(_amx.get(), index, name, nullptr);
#else
		return amx_GetPublic(_amx.get(), index, name);
#endif
	}

	inline int GetPubVar(int index, char* name, cell** address) {
#ifdef MP_AMX4
		return amx_GetPubVar(_amx.get(), index, name, address);
#else
		cell amx_addr{};

		int err = amx_GetPubVar(_amx.get(), index, name, &amx_addr);
		if (err == AMX_ERR_NONE)
		{
			*address = reinterpret_cast<cell*>(Data() + amx_addr);
		}

		return err;
#endif
	}
	inline int GetPubVar(int index, char* name, cell* amx_address) {
#ifdef MP_AMX4
		auto err = amx_GetPubVar(_amx.get(), index, name, &amx_address);
		if (err == AMX_ERR_NONE)
		{
			*amx_address = *reinterpret_cast<cell*>(amx_address - reinterpret_cast<unsigned>(Data()));
		}

		return err;
#else
		return amx_GetPubVar(_amx.get(), index, name, amx_address);
#endif
	}

	inline int GetTag(int index, char* tag_name, cell* tag_id) { return amx_GetTag(_amx.get(), index, tag_name, tag_id); }
	inline int GetUserData(long tag, void** ptr) { return amx_GetUserData(_amx.get(), tag, ptr); }
	inline int Init(void* program) { return amx_Init(_amx.get(), program); }
	inline int InitJIT(void* reloc_table, void* native_code) { return amx_InitJIT(_amx.get(), reloc_table, native_code); }
	inline int MemInfo(long* codesize, long* datasize, long* stackheap) { return amx_MemInfo(_amx.get(), codesize, datasize, stackheap); }
	inline int NameLength(int* length) { return amx_NameLength(_amx.get(), length); }
	inline int NumNatives(int* number) { return amx_NumNatives(_amx.get(), number); }
	inline int NumPublics(int* number) { return amx_NumPublics(_amx.get(), number); }
	inline int NumPubVars(int* number) { return amx_NumPubVars(_amx.get(), number); }
	inline int NumTags(int* number) { return amx_NumTags(_amx.get(), number); }
	inline int Push(cell value) { return amx_Push(_amx.get(), value); }
	inline int PushAddress(cell* address)
	{
#ifdef MP_AMX4
		return amx_PushAddress(_amx.get(), address);
#else
		cell push_addr = reinterpret_cast<cell>(reinterpret_cast<unsigned char*>(address) - Data());
		return amx_Push(_amx.get(), push_addr);
#endif
	}
	inline int PushArray(cell** address, const cell arr[], unsigned numcells) {
#ifdef MP_AMX4
		return amx_PushArray(_amx.get(), address, arr, numcells);
#else
		return amx_PushArray(_amx.get(), nullptr, address, arr, numcells);
#endif
	}
	inline int PushArray(cell* amx_addr, cell** phys_addr, const cell arr[], unsigned numcells) {
#ifdef MP_AMX4
		auto err = amx_PushArray(_amx.get(), phys_addr, arr, numcells);
		if (err == AMX_ERR_NONE && amx_addr != nullptr)
		{
			*amx_addr = reinterpret_cast<cell>(reinterpret_cast<unsigned char*>(*phys_addr) - Data());
		}
		return err;
#else
		return amx_PushArray(_amx.get(), amx_addr, phys_addr, arr, numcells);
#endif
	}

	inline int PushString(cell** address, const char* string, int pack, int use_wchar) {
#ifdef MP_AMX4
		return amx_PushString(_amx.get(), address, string, pack, use_wchar);
#else
		return amx_PushString(_amx.get(), nullptr, address, string, pack, use_wchar);
#endif
	}
	inline int PushArray(cell* amx_addr, cell** phys_addr, const char* string, int pack, int use_wchar) {
#ifdef MP_AMX4
		auto err = amx_PushString(_amx.get(), phys_addr, string, pack, use_wchar);
		if (err == AMX_ERR_NONE && amx_addr != nullptr)
		{
			*amx_addr = reinterpret_cast<cell>(reinterpret_cast<unsigned char*>(*phys_addr) - Data());
		}
		return err;
#else
		return amx_PushString(_amx.get(), amx_addr, phys_addr, string, pack, use_wchar);
#endif
	}

	inline int RaiseError(int error) { return amx_RaiseError(_amx.get(), error); }
	inline int Register(const AMX_NATIVE_INFO* nativelist, int number) { return amx_Register(_amx.get(), nativelist, number); }
	inline int Release(cell* address) {
#ifdef MP_AMX4
		return amx_Release(_amx.get(), address);
#else
		return amx_Release(_amx.get(), reinterpret_cast<cell>(reinterpret_cast<unsigned char*>(address) - Data()));
#endif
	}
	inline int Release(cell address) {
#ifdef MP_AMX4
		return amx_Release(_amx.get(), reinterpret_cast<cell*>(address + Data()));
#else
		return amx_Release(_amx.get(), address);
#endif
	}
	inline int SetCallback(AMX_CALLBACK callback) { return amx_SetCallback(_amx.get(), callback); }
	inline int SetDebugHook(AMX_DEBUG debug) { return amx_SetDebugHook(_amx.get(), debug); }
	inline int SetUserData(long tag, void* ptr) { return amx_SetUserData(_amx.get(), tag, ptr); }
#pragma endregion

	inline cell* GetAddress(cell amx_address)
	{
#ifdef MP_AMX4
		return amx_Address(_amx.get(), amx_address);
#else
		cell* phys_addr;
		if (amx_GetAddr(_amx.get(), amx_address, &phys_addr) != AMX_ERR_NONE)
			return nullptr;

		return phys_addr;
#endif
	}

	inline cell PushAll() { return HEA(); }

	template<class T, class... Args>
	cell PushAll(T value, Args&&... args)
	{
		// Save the first address of the heap and return it when the function ends.
		// Can be used to clear the heap after executing public functions
		cell current_hea = HEA();

		// Recursion must be done so arguments are pushed in order
		PushAll(args...);

		if constexpr (std::is_integral_v<T>)
		{
			Push(value);
		}
		else if constexpr (std::is_floating_point_v<T>)
		{
			Push(amx::cast<cell>(value));
		}
		else if constexpr (std::is_same_v<std::remove_const_t<std::remove_reference_t<T>>, std::string>)
		{
			PushString(nullptr, value.c_str(), 0, 0);
		}
		else if constexpr (std::is_same_v<std::add_pointer_t<std::remove_const_t<std::remove_pointer_t<T>>>, char*>)
		{
			PushString(nullptr, value, 0, 0);
		}
		else
		{
			static_assert(std::is_convertible_v<T, cell>, "Value must be castable to cell to be pushed");
			Push(static_cast<cell>(value));
		}

		return current_hea;
	}

	static std::string GetString(const cell* amx_str);
	bool SetPublicVariable(const std::string_view varname, cell value);

	class ScopeHeap
	{
		CAMX* _amx;
		cell _hea;

	public:
		explicit ScopeHeap(CAMX* amx) : _amx(amx), _hea(_amx->HEA()) {}
		~ScopeHeap() { _amx->Release(_hea); }
	};
};