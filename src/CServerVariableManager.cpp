#include "main.hpp"

void CServerVariableManager::Delete(const std::string& key)
{
	std::erase_if(_svars, [&](const std::pair<std::string, VarType>& v) {
		return v.first == key;
	});
}

bool CServerVariableManager::Exists(const std::string& key)
{
	const auto it = std::find_if(_svars.begin(), _svars.end(), [&](const std::pair<std::string, VarType>& v) {
		return v.first == key;
		});

	if (it == _svars.end())
		return false;

	return true;
}

CServerVariableManager::var_type CServerVariableManager::GetType(const std::string& key)
{
	const auto it = std::find_if(_svars.begin(), _svars.end(), [&](const std::pair<std::string, VarType>& v) {
		return v.first == key;
		});

	if (it == _svars.end())
		return var_type_none;

	if (std::holds_alternative<float>(it->second))
		return var_type_float;
	
	if (std::holds_alternative<std::string>(it->second))
		return var_type_string;

	return var_type_int;
}

std::string CServerVariableManager::GetNameAtIndex(std::size_t index)
{
	try
	{
		return _svars.at(index).first;
	}
	catch (const std::out_of_range& e)
	{
		return std::string();
	}
}

void CServerVariableManager::Delete(std::size_t playerid, const std::string& key)
{
	std::erase_if(_pvars[playerid], [&](const std::pair<std::string, VarType>& v) {
		return v.first == key;
	});
}

bool CServerVariableManager::Exists(std::size_t playerid, const std::string& key)
{
	const auto it = std::find_if(_pvars[playerid].begin(), _pvars[playerid].end(), [&](const std::pair<std::string, VarType>& v) {
		return v.first == key;
		});

	if (it == _pvars[playerid].end())
		return false;

	return true;
}

CServerVariableManager::var_type CServerVariableManager::GetType(std::size_t playerid, const std::string& key)
{
	const auto it = std::find_if(_pvars[playerid].begin(), _pvars[playerid].end(), [&](const std::pair<std::string, VarType>& v) {
		return v.first == key;
	});

	if (it == _pvars[playerid].end())
		return var_type_none;

	if (std::holds_alternative<float>(it->second))
		return var_type_float;
	else if (std::holds_alternative<std::string>(it->second))
		return var_type_string;

	return var_type_int;
}

std::string CServerVariableManager::GetNameAtIndex(std::size_t playerid, std::size_t index)
{
	try
	{
		return _pvars[playerid].at(index).first;
	}
	catch (const std::out_of_range& e)
	{
		return std::string();
	}
}

std::unique_ptr<CServerVariableManager> mp::var_manager = std::make_unique<CServerVariableManager>();