#include "main.hpp"

std::unique_ptr<CConsole> mp::console = std::make_unique<CConsole>();

std::optional<std::reference_wrapper<CConsole::stConVar>> CConsole::GetVariable(const std::string& varname)
{
	return (_vars.contains(varname) ? std::make_optional(std::ref(_vars[varname])) : std::nullopt);
}