#include "main.hpp"

static unsigned GetNextNumbers(const std::string_view str, std::size_t& idx)
{
	unsigned n = 0U;
	++idx;

	while ((str[idx] - '0') <= 9)
	{
		n = 10 * n + (str[idx] - '0');
		++idx;
	}

	return n < 0U ? 0U : n;
}

std::string mp::format::format_string(std::shared_ptr<CScript>& script, const cell* params, int format_idx, int params_idx)
{
	unsigned param_count = params[0] / sizeof(cell);

	if (param_count < format_idx)
		return std::string();

	std::string format_string = script->AMX()->GetString(script->AMX()->GetAddress(params[format_idx]));

	if (param_count < params_idx)
		return format_string;

	mp::utils::replace_chars(format_string, "{", "{{");
	mp::utils::replace_chars(format_string, "}", "}}");

	std::bitset<format_flags::flag_count> flags;
	unsigned width;
	unsigned precision;
	char current_char{ '\0' };
	std::size_t current_argument{ 0U };
	std::size_t selected_argument{ UINT_MAX };
	std::size_t idx{ 0U };
	std::string current_specifier;
	std::string result;
	fmt::dynamic_format_arg_store<fmt::format_context> argument_store;

	while (true)
	{
	loop_again:
		for (; idx < format_string.length() && (current_char = format_string[idx]) != '%'; ++idx)
		{
			result += current_char;
		}

		if (idx >= format_string.length())
			break;

		width = 0U;
		precision = 0U;
		selected_argument = UINT_MAX;
		flags.reset();

	bump_char:
		current_char = format_string[++idx];

		if (idx >= format_string.length())
			break;

	switch_spec:
		#define THROW_SPEC_ERROR() throw std::runtime_error{ fmt::format("Invalid parameter passed to '%{:c}' specifier.", current_char) }
		#define THROW_MALFORMED_ERROR() throw std::runtime_error{ "Malformed specifier" }
		#define THROW_RANGE_ERROR() throw std::runtime_error{ fmt::format("Attempted to access value {} with {} parameters passed.", params_idx, param_count) }
		
		switch (current_char)
		{
			case '-':
			case '<':
			{
				flags.set(format_flags::left_alignment);
				goto bump_char;
			}
			case '>':
			{
				flags.set(format_flags::right_alignment);
				goto bump_char;
			}
			case '^':
			{
				flags.set(format_flags::centered);
				goto bump_char;
			}
			case '*':
			{
				if (param_count < params_idx)
				{
					THROW_RANGE_ERROR();
				}

				width = *script->AMX()->GetAddress(params[params_idx++]);
				goto bump_char;
			}
			case '.':
			{
				if (format_string.length() <= idx + 1)
				{
					THROW_MALFORMED_ERROR();
				}

				if (format_string[idx + 1] == '*')
				{
					if (param_count < params_idx)
					{
						THROW_RANGE_ERROR();
					}

					precision = *script->AMX()->GetAddress(params[params_idx++]);
					idx += 2;
				}
				else
				{
					precision = GetNextNumbers(format_string, idx);
				}

				current_char = format_string[idx];
				goto switch_spec;
			}
			case '@':
			{
				selected_argument = GetNextNumbers(format_string, idx);
				current_char = format_string[idx];
				goto switch_spec;
			}
			case ',':
			{
				goto bump_char;
			}
			case '0':
			{
				flags.set(format_flags::zero_padding);
				goto bump_char;
			}
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			{
				width = GetNextNumbers(format_string, idx);
				current_char = format_string[idx];
				goto switch_spec;
			}
			case '#':
			{
				flags.set(format_flags::alternative_formatting);
				goto bump_char;
			}
			case 's':
			{
				if (selected_argument == UINT_MAX)
				{
					if (param_count < params_idx)
						THROW_RANGE_ERROR();

					argument_store.push_back(script->AMX()->GetString(script->AMX()->GetAddress(params[params_idx++])));
				}

				break;
			}
			case 'i':
			{
				current_char = 'd';
				[[fallthrough]];
			}
			case 'd':
			case 'x':
			case 'X':
			case 'b':
			case 'B':
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'o':
			case 'c':
			case 'f':
			case 'F':
			{
				if (selected_argument == UINT_MAX)
				{
					if (param_count < params_idx)
						THROW_RANGE_ERROR();

					cell value = *script->AMX()->GetAddress(params[params_idx++]);
					if (current_char == 'f' || current_char == 'F')
					{
						argument_store.push_back(amx::cast<float>(value));
					}
					else
					{
						argument_store.push_back(value);
					}
				}

				break;
			}
			case '%':
			{
				result += '%';
				++idx;
				goto loop_again;
			}
			default:
			{
				// custom formatters
				break;
			}
		}

		#undef THROW_SPEC_ERROR
		#undef THROW_MALFORMED_ERROR
		#undef THROW_RANGE_ERROR

		current_specifier = '{';
		current_specifier += (selected_argument != UINT_MAX ? fmt::to_string(selected_argument) : fmt::to_string(current_argument++));
		current_specifier += ':';

		if (flags.test(format_flags::alternative_formatting))
			current_specifier += '#';

		if (flags.test(format_flags::zero_padding))
			current_specifier += '0';

		if (flags.test(format_flags::left_alignment))
			current_specifier += '<';
		else if (flags.test(format_flags::right_alignment))
			current_specifier += '>';
		else if (flags.test(format_flags::centered))
			current_specifier += '^';

		if (width != 0U)
			current_specifier += fmt::to_string(width);

		if (precision != 0U)
			current_specifier += fmt::format(".{}", precision);

		current_specifier += current_char;
		current_specifier += '}';

		result += current_specifier;
		++idx;
	}

	return fmt::vformat(result, argument_store);
}
