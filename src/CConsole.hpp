#pragma once

/* 
**
** Console Emulator
**
*/

class CConsole
{
public:
	enum var_flags {
		none = 0,
		read_only = 1 << 0,
		debug = 1 << 1,
		rule = 1 << 2
	};

	struct stConVar
	{
		std::variant<int, bool, float, std::string> value;
		unsigned flags;
	};

private:
	std::unordered_map<std::string, stConVar> _vars;

public:
	CConsole() = default;
	~CConsole() = default;

	template<class T>
	void SetVariable(const std::string& varname, const T& value, unsigned flags = var_flags::none)
	{
		_vars[varname] = { value, flags };
	}

	std::optional<std::reference_wrapper<stConVar>> GetVariable(const std::string& varname);
};

namespace mp {
	extern std::unique_ptr<CConsole> console;
}