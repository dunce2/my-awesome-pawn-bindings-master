#include "main.hpp"
#include "CScriptManager.hpp"

std::unique_ptr<CScriptManager> mp::script_manager = std::make_unique<CScriptManager>();

CScriptManager::CScriptManager() // NOLINT
{
	_scripts[0].reset();
}

unsigned CScriptManager::Load(const std::string& name, bool filterscript)
{
	std::filesystem::path script_path = std::filesystem::current_path() / "pawn" / (filterscript ? "filterscripts" : "gamemodes") / name;
	if (!script_path.has_extension())
		script_path.replace_extension(".amx");


	mp::log->info("[ScriptManager] Loading {} \"{}\"...", (filterscript ? "filterscript" : "gamemode"), script_path.stem().string());
	std::shared_ptr<CScript> script;

	try
	{
		script = std::make_shared<CScript>(script_path, filterscript);
	}
	catch (const std::exception& e)
	{
		mp::log->error("[ScriptManager] Failed to load script \"{}\".", name);
		mp::log->error("[ScriptManager] Exception details:");
		mp::log->error("[ScriptManager]    {}", e.what());

		return 0xFFFFFFFF;
	}

	unsigned script_id = (filterscript ? _highest_id++ : 0U);
	_scripts[script_id] = script;

	if (mp::config::features.test(mp::config::config_features::jit_compilation))
	{
		int err = script->AMX()->Compile();
		if (err != AMX_ERR_NONE)
		{
			mp::log->error("[script:{}] error while jit-compiling: {}", script->Name(), CAMX::GetErrorMessage(err));
		}
	}

	mp::log->info("[ScriptManager] Loaded with ID {}", script_id);

	if (filterscript)
	{
		script->CallFunction("OnFilterScriptInit", nullptr);
	}
	else
	{
		Call<call_filterscripts, 0>("OnGameModeInit", nullptr);
		script->CallFunction("OnGameModeInit", nullptr);
	}

	script->AMX()->Exec(nullptr, AMX_EXEC_MAIN);

	return script_id;
}

void CScriptManager::Unload(const std::string& name)
{
	auto it = std::find_if(_scripts.begin(), _scripts.end(), [&](const std::pair<unsigned, std::shared_ptr<CScript>>& s) {
		return s.second->Name() == name;
	});

	if (it == _scripts.end())
	{
		mp::log->error("[ScriptManager] Attempted to unload non-existing script \"{}\".", name);
		return;
	}

	auto& [id, script] = *it;

	mp::log->info("[ScriptManager] Unloading {} \"{}\" (ID {})...", (script->FilterScript() ? "filterscript" : "gamemode"), script->Name(), id);

	if (script->FilterScript())
	{
		script->CallFunction("OnFilterScriptExit", nullptr);
	}
	else
	{
		Call<call_mode::call_gamemode_first, 0>("OnGameModeExit");
	}

	script->Cleanup();

	_scripts.erase(id);

	mp::log->info("[ScriptManager] Script \"{}\" unloaded.", name);
}

void CScriptManager::Unload(std::size_t id)
{
	try
	{
		std::shared_ptr<CScript>& script = _scripts.at(id);

		mp::log->info("[ScriptManager] Unloading {} \"{}\" (ID {})...", (script->FilterScript() ? "filterscript" : "gamemode"), script->Name(), id);

		if (script->FilterScript())
		{
			script->CallFunction("OnFilterScriptExit", nullptr);
		}
		else
		{
			Call<call_mode::call_gamemode_first, 0>("OnGameModeExit");
		}

		script->Cleanup();

		_scripts.erase(id);

		mp::log->info("[ScriptManager] Unloaded.");
	}
	catch (const std::out_of_range& e)
	{
		mp::log->error("[ScriptManager] Attempted to unload non-existing script with ID \"{}\".", id);
		return;
	}
}

void CScriptManager::UnloadAll()
{
	for (auto it = std::next(_scripts.begin()); it != _scripts.end(); ++it)
	{
		Unload(it->first);
	}

	Unload(gamemode_id);
}
