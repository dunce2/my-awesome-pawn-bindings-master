#include "main.hpp"

CHashWork* mp::current_hash_work = nullptr;

void mp::crypto::DoHash(uv_work_t* handle)
{
	auto* work = static_cast<CHashWork*>(handle->data);

	auto result = work->_hash_function->process(work->_hash_data);
	work->_hash_result = Botan::hex_encode(result);
}

void mp::crypto::AfterHash(uv_work_t* handle, int status)
{
	if (status != UV_ECANCELED)
	{
		auto* work = static_cast<CHashWork*>(handle->data);

		CAMX::ScopeHeap rel(work->_script->AMX());
		work->_script->PushParamVector(work->_callback_params);

		work->_script->GC()->GetSymbol(work).ref_count--;
		work->_callback_called = true;

		mp::current_hash_work = work;
		work->_script->CallFunction(work->_callback_idx, nullptr);
		mp::current_hash_work = nullptr;
	
		work->_work_handle = nullptr;
	}

	delete handle;
}

CHashWork::CHashWork(const std::shared_ptr<CScript>& script, const std::string& hash_algo)
	: _script(script)
{
	_hash_function = Botan::HashFunction::create_or_throw(hash_algo);
	_work_handle = new uv_work_t;
	_work_handle->data = static_cast<void*>(this);
}

CHashWork::~CHashWork()
{
	if (_work_handle != nullptr && !_callback_called)
	{
		uv_cancel(reinterpret_cast<uv_req_t*>(_work_handle));
	}
}

bool CHashWork::SetCallback(const cell* params, int callback_idx, int format_idx, int params_idx)
{
	unsigned param_count = params[0] / sizeof(cell);

	if (param_count < callback_idx)
		return false;

	std::string callback_name = CAMX::GetString(_script->AMX()->GetAddress(params[callback_idx]));
	if (callback_name.empty())
		return false;

	if (_script->AMX()->FindPublic(callback_name.c_str(), &_callback_idx) != AMX_ERR_NONE)
		return false;

	if (param_count >= params_idx)
	{
		_script->ExtractParams(params, format_idx, params_idx, _callback_params, true);
	}

	return true;
}

void CHashWork::RunThreaded()
{
	_callback_called = false;

	if (_work_handle == nullptr)
	{
		_work_handle = new uv_work_t;
		_work_handle->data = static_cast<void*>(this);
	}

	_script->GC()->GetSymbol(this).ref_count++;

	mp::log->debug("[hash:{}] Executing hash function \"{}\".", fmt::ptr(this), _hash_function->name());
	uv_queue_work(uv_default_loop(), _work_handle, mp::crypto::DoHash, mp::crypto::AfterHash);
}
