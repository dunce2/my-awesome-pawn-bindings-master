#pragma once

class CMongPawnServer : public rm::server {
public:
	void OnLoad() override;
	void OnUpdate() override;
	void OnUnload() override;

	bool OnPlayerConnect(rm::player* player) override;
	/*
	bool OnPlayerDisconnect(rm::player* player, int reason) override;
	bool OnPlayerRequestClass(rm::player* player, unsigned short classid) override;
	bool OnPlayerRequestSpawn(rm::player* player) override;
	bool OnPlayerDeath(rm::player* victim, rm::player* killer, unsigned char reason) override;
	bool OnPlayerUpdate(rm::player* player) override;
	bool OnPlayerEnterVehicle(rm::player* player, rm::vehicle* vehicle, bool ispassenger) override;
	bool OnPlayerExitVehicle(rm::player* player, rm::vehicle* vehicle) override;
	bool OnPlayerWeaponShot(rm::player* player, unsigned char weaponid, unsigned char hittype, unsigned short hitid, float coords[3]) override;
	bool OnPlayerDamage(rm::player* to, rm::player* by, float amount, unsigned char weaponid, unsigned char bodypart) override;
	bool OnPlayerText(rm::player* player, const std::string& text) override;
	bool OnPlayerDriveBy(rm::player* player, bool driveby) override;
	bool OnVehicleDeath(rm::vehicle* vehicle, rm::player* reporter) override;
	bool OnVehicleMod(rm::player* player, rm::vehicle* vehicle, unsigned short componentid) override;
	bool OnEnterExitModShop(rm::player* player,bool enterexit, unsigned char interiorid) override;
	bool OnVehiclePaintjob(rm::player* player, rm::vehicle* vehicle, unsigned char paintjobid) override;
	bool OnVehicleRespray(rm::player* player, rm::vehicle* vehicle, unsigned char color1, unsigned char color2) override;
	bool OnVehicleDamageStatusUpdate(rm::vehicle* vehicle, rm::player* player, const rm::vehicle_damage_status& status) override;
	bool OnUnoccupiedVehicleUpdate(rm::vehicle* vehicle, rm::player* player, unsigned char passengerseat, const rm::vector<>& newposition, const rm::vector<>& velocity) override;
	bool OnTrailerUpdate(rm::player* player, rm::vehicle* vehicle) override;
	bool OnVehicleSirenStateChange(rm::player* player, rm::vehicle* vehicle, bool newstate) override;
	*/
};