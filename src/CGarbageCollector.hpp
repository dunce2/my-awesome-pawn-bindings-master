#pragma once

#define CImontash CGarbageCollector

class CGarbageCollector
{
public:
	struct symbol
	{
		std::function<void(void*)> destructor;
		int ref_count;
	};

private:
	std::unordered_map<void*, symbol> _collection;

public:
	~CGarbageCollector();

	template<class T>
	void Handle(T* ptr, std::function<void(T*)> destructor)
	{
		decltype(symbol::destructor) destructor_fun = [=](void* p) { destructor(static_cast<T*>(p)); };
		_collection[static_cast<void*>(ptr)] = { std::move(destructor_fun), 0U };
	}

	template<class T>
	inline symbol& GetSymbol(T* ptr) { return _collection.at(static_cast<void*>(ptr)); }
	template<class T>
	inline const symbol& GetSymbol(T* ptr) const { return _collection.at(static_cast<void*>(ptr)); }

	template<class T>
	inline bool Exists(T* ptr) const { return _collection.contains(static_cast<void*>(ptr)); }

	void Collect();
};