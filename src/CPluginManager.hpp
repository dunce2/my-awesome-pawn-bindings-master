#pragma once

namespace mp::plugins 
{
	enum SUPPORTS_FLAGS
	{
		SUPPORTS_VERSION = 0x0200,
		SUPPORTS_VERSION_MASK = 0xFFFF,
		SUPPORTS_AMX_NATIVES = 0x10000,
		SUPPORTS_PROCESS_TICK = 0x20000
	};

	enum PLUGIN_DATA_TYPE
	{
		PLUGIN_DATA_LOGPRINTF = 0x00,
		PLUGIN_DATA_AMX_EXPORTS = 0x10,
		PLUGIN_DATA_CALLPUBLIC_FS = 0x11,
		PLUGIN_DATA_CALLPUBLIC_GM = 0x12,
		PLUGIN_DATA_NETGAME = 0xE1,
		PLUGIN_DATA_RAKSERVER = 0xE2,
		PLUGIN_DATA_LOADFSCRIPT = 0xE3,
		PLUGIN_DATA_CONSOLE = 0xE4,
		PLUGIN_DATA_UNLOADFSCRIPT = 0xE5
	};

	enum PLUGIN_AMX_EXPORT
	{
		PLUGIN_AMX_EXPORT_Align16 = 0,
		PLUGIN_AMX_EXPORT_Align32 = 1,
		PLUGIN_AMX_EXPORT_Align64 = 2,
		PLUGIN_AMX_EXPORT_Allot = 3,
		PLUGIN_AMX_EXPORT_Callback = 4,
		PLUGIN_AMX_EXPORT_Cleanup = 5,
		PLUGIN_AMX_EXPORT_Clone = 6,
		PLUGIN_AMX_EXPORT_Exec = 7,
		PLUGIN_AMX_EXPORT_FindNative = 8,
		PLUGIN_AMX_EXPORT_FindPublic = 9,
		PLUGIN_AMX_EXPORT_FindPubVar = 10,
		PLUGIN_AMX_EXPORT_FindTagId = 11,
		PLUGIN_AMX_EXPORT_Flags = 12,
		PLUGIN_AMX_EXPORT_GetAddr = 13,
		PLUGIN_AMX_EXPORT_GetNative = 14,
		PLUGIN_AMX_EXPORT_GetPublic = 15,
		PLUGIN_AMX_EXPORT_GetPubVar = 16,
		PLUGIN_AMX_EXPORT_GetString = 17,
		PLUGIN_AMX_EXPORT_GetTag = 18,
		PLUGIN_AMX_EXPORT_GetUserData = 19,
		PLUGIN_AMX_EXPORT_Init = 20,
		PLUGIN_AMX_EXPORT_InitJIT = 21,
		PLUGIN_AMX_EXPORT_MemInfo = 22,
		PLUGIN_AMX_EXPORT_NameLength = 23,
		PLUGIN_AMX_EXPORT_NativeInfo = 24,
		PLUGIN_AMX_EXPORT_NumNatives = 25,
		PLUGIN_AMX_EXPORT_NumPublics = 26,
		PLUGIN_AMX_EXPORT_NumPubVars = 27,
		PLUGIN_AMX_EXPORT_NumTags = 28,
		PLUGIN_AMX_EXPORT_Push = 29,
		PLUGIN_AMX_EXPORT_PushArray = 30,
		PLUGIN_AMX_EXPORT_PushString = 31,
		PLUGIN_AMX_EXPORT_RaiseError = 32,
		PLUGIN_AMX_EXPORT_Register = 33,
		PLUGIN_AMX_EXPORT_Release = 34,
		PLUGIN_AMX_EXPORT_SetCallback = 35,
		PLUGIN_AMX_EXPORT_SetDebugHook = 36,
		PLUGIN_AMX_EXPORT_SetString = 37,
		PLUGIN_AMX_EXPORT_SetUserData = 38,
		PLUGIN_AMX_EXPORT_StrLen = 39,
		PLUGIN_AMX_EXPORT_UTF8Check = 40,
		PLUGIN_AMX_EXPORT_UTF8Get = 41,
		PLUGIN_AMX_EXPORT_UTF8Len = 42,
		PLUGIN_AMX_EXPORT_UTF8Put = 43
	};

	extern "C" void* GetNetGame();
	extern "C" CConsole* GetConsole();
	extern "C" bool UnloadFilterscript(char* filename);
	extern "C" bool LoadFilterscript(char* filename, char* filedata);
	extern "C" int CallPublicFS(char* function_name);
	extern "C" int CallPublicGM(char* function_name);
}
class CPluginManager {
public:
	struct stPlugin {
		using Load_t = bool(*)(void**);
		using Unload_t = void(*)();
		using AmxLoad_t = int(*)(AMX*);
		using AmxUnload_t = int(*)(AMX*);
		using ProcessTick_t = void(*)();
		using Supports_t = unsigned(*)();

		uv_lib_t plugin_handle;
		Load_t Load{ nullptr };
		Unload_t Unload{ nullptr };
		AmxLoad_t AmxLoad{ nullptr };
		AmxUnload_t AmxUnload{ nullptr };
		ProcessTick_t ProcessTick{ nullptr };
		Supports_t Supports{ nullptr };
	};

private:
	std::array<void*, 44> _amx_exports;
	std::array<void*, 0xE6> _plugin_exports;

	std::unordered_map<std::string, std::unique_ptr<stPlugin>> _plugins;

public:
	CPluginManager();
	~CPluginManager();

	bool LoadPlugin(const std::string& plugin_name);
	void UnloadPlugin(const std::string& plugin_name);

	void AmxLoad(AMX* amx);
	void AmxUnload(AMX* amx);
	void ProcessTick();
};