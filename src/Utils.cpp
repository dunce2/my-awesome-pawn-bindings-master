#include "main.hpp"
#include "Utils.hpp"

void mp::utils::replace_chars(std::string& string, const std::string_view find, const std::string_view replace)
{
	std::size_t idx{ 0U };
	while ((idx = string.find(find, idx)) != std::string::npos)
	{
		string.replace(idx, find.length(), replace);
		idx += replace.length();
	}
}

void mp::utils::call_evloop_sync(const std::function<void()>& fun)
{
	std::promise<void> await_promise;

	mp::evloop_task_queue->push([&]() {
		fun();
		await_promise.set_value();
	});

	await_promise.get_future().wait();
}